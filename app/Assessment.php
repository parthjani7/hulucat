<?php

namespace App;

class Assessment extends Model
{
    protected $fillable=['name','total_questions','status'];

    public function questions(){
      return $this->hasMany(AssessmentData::class);
    }
    public function users(){
			return $this->belongsToMany(User::class,'user_assessments');

		}
}
