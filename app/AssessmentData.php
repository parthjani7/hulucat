<?php

namespace App;

class AssessmentData extends Model
{
		protected $table='assessment_data';
    public $fillable=['question','answer','question_type'];

		public function Assessment(){
			return $this->belongsTo(Assessment::class);
		}
		public function useranswer()
		{
			return $this->hasOne('App\AssessmentDataAnswer');
		}
}
