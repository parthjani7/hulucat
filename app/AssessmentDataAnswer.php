<?php

namespace App;

class AssessmentDataAnswer extends Model
{
	protected $table='assessment_data_answers';
	public $fillable=['user_id','assessment_data_id','is_true','answer'];

	public function question()
	{
		return $this->hasOne('App\AssessmentDataAnswers');
	}
}
