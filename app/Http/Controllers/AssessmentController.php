<?php

namespace App\Http\Controllers;

use App\Assessment;
use App\AssessmentData;
use App\User;
use Illuminate\Http\Request;

class AssessmentController extends CommonController
{
	public function __construct(){
		$this->middleware('role:Administrator');
	}

	public function index(){
		return view('pages.assessment.list',['assessments'=>Assessment::get()]);
	}

	public function create(){
		return view('pages.assessment.create');
	}

	public function store(Request $request){

		$this->validate($request,[
			'name'=>'required|min:2',
			'status'=>'required',
		]);
		if(count($request->data)<1){
			return redirect()->back()->withErrors(['You must enter at least 1 question..!']);
		}

		$ref=Assessment::create([
			'name'=>$request->name,
			'status'=>$request->status,
			'total_questions'=>count($request->data),
		]);

		$this->saveAssessmentData($ref,$request);

		return redirect('/assessments');
	}


	public function edit(Assessment $assessment)
	{
		return view('pages.assessment.edit',[
			'assessment'=>$assessment,
			'questions'=>$assessment->questions
		]);
	}

	public function update(Request $request,Assessment $assessment)
	{

		$this->validate($request,[
			'name'=>'required|min:2',
			'status'=>'required',
		]);
		if(count($request->data)<1){
			return redirect()->back()->withErrors(['You must enter at least 1 question..!']);
		}

		$assessment->name=$request->name;
		$assessment->status=$request->status;
		$assessment->total_questions=count($request->data);
		$assessment->save();
		$assessment->Questions()->delete();

		$this->saveAssessmentData($assessment,$request);

		return redirect('/assessments');

	}

	protected function saveAssessmentData($assessment,$request)
	{
		foreach ($request->data as $key=>$record) {

			$a_data=new AssessmentData();
			$question=[];
			$a_data->question_type=$record['que_type'][0];
			$question['que'][]=$record['que'][0];

			if($record['que_type'][0]=='0'){

				$a_data->answer=$record['option'][$request->answers[$key]];

				foreach ($record['option'] as $option)
					$question['options'][]=$option;

			}else
				$a_data->answer=$record['answer'][0];

			$a_data->question=json_encode($question);
			$assessment->Questions()->save($a_data);
		}
	}

	public function assignAssessment(){
		try {
			return view('pages.assessment.assign',[
				'users'=>$this->getUsersInfo(),
				'assessmentlist'=>Assessment::where('status','1')->get(),
			]);
		} catch (Exception $e) {
			return back()
				->with('message',$e->getMessage());
		}
	}
	public function updateAssignment(Request $request){
		try {

			$assessment_ids=$request->assessment_ids;
			if($assessment_ids!=null){
				$users=User::whereNotIn('id',array_keys($assessment_ids))->get();
				foreach ($assessment_ids as $uid => $assessment_id) {
					User::find($uid)->Assessments()->sync($assessment_id);
				}
			}else{ $users=User::get(); }
			$users->each(function($e){
				$e->Assessments()->detach();
			});
			return back()
				->with('message','Assessments assigned successfully..!');
		} catch (Exception $e) {
			return back()
				->with('message',$e->getMessage());
		}
	}
	public function resultList(){
		try {

			$users=User::where('status',1)
				->get()->filter(function($u){
				return ($u->assessments->count())?$u:'';
			});
			return view('pages.assessment.resultlist',['assessment'=>$users]);
		} catch (Exception $e) {
			return back()
				->with('message',$e->getMessage());
		}
	}

	public function toggle(Assessment $assessment)
	{
		try {
			$assessment->update(['status'=>(!$assessment->status)]);
			return \Response::json(array('resultCode'=>'success','status'=>$assessment->status));
		} catch (\Exception $e) {
			echo $e->getMessage();
		}
	}

	public function getResult(User $user,Assessment $assessment){
		$find_assessment=$user->assessments()->where('id',$assessment->id)->where('is_taken','1')->first();
		if($find_assessment==null)
			return \Response::json(array('resultCode'=>'warning','resultMessage'=>'Assessment has not been taken by this user yet'));

		$answers=[];
		$find_assessment->Questions()->get()->each(function($e) use(&$answers){
			$id=$e->useranswer()->pluck('assessment_data_id')->first();
			$user_answer=$e->useranswer()->pluck('answer')->first();
			$answers[$id]=[
				'user_answer'=>$user_answer,
				'correct_answer'=>$e->answer,
				'is_true'=>$e->useranswer()->pluck('is_true')->first(),
			];

		});
		//dd($answers);
		$questions=$find_assessment->Questions()->get();
		$data=collect();
		$question=$options='';
		$counter=0;
		foreach ($questions as $record) {
			$decode=json_decode($record->question);
			$counter++;
			$is_true=($answers[$record->id]['is_true']==1)?"True":"False";
			$textType=($answers[$record->id]['is_true']==1)?"success":"danger";
			$question="<div class='question'><div class='row form-group'><div class='col-md-12'>
			<label class='text-$textType'>".$is_true."</label>
			<label class='pull-right'><a onclick='updateAnswer(".$record->id.",".$user->id.")' href='javascript:void(0)'>Mark Ture/False</a></label>
			<div class='input-group'> <div class='input-group-addon que_num'>".$counter."</div><input class='form-control' disabled value='".$decode->que[0]."' type='text'> </div></div></div><div class='form-group mcq'> <div class='row'>";
			if(isset($decode->options)){
				foreach ($decode->options as $option) {
					$question.="<div class='col-md-3'><label> <input type='radio' class='radio_answer' ";
					if($answers[$record->id]['user_answer']==$option){
						$question.='checked';
					}else{
						$question.='disabled';
					}
					$question.=" > &nbsp;".($option)."</label></div>";
				}
			}else{
				$question.="<div class='col-sm-12'><div class='form-group'><input class='form-control blank_answer' type='text' value='".$answers[$record->id]['user_answer']."'></div></div>";
			}

			$question.="</div></div><div class='row form-group'><div class='col-md-12'><div class='input-group'> <div class='input-group-addon que_num'>Correct Answer</div><input class='form-control' disabled value='".$record->answer."' type='text'> </div></div></div></div><hr/>";

				$data->push($question);
		}

		return \Response::json(
			array(
			'resultCode'=>'success',
			'resultValue'=>array(
				'user'=>$user->fullname,
				'data'=>$data,
				)
			)
		);
	}

	public function toggleAnswer(Request $request)
	{

		try {
			$answer=\DB::table('assessment_data_answers')->
				where('user_id',$request->input('user'))->
				where('assessment_data_id',$request->input('adata'));
			$answer->update([
				'is_true'=>(!$answer->first()->is_true)
			]);

			return \Response::json(array('resultCode'=>'success','status'=>$answer->first()->is_true));
		} catch (\Exception $e) {
			echo $e->getMessage();
		}
	}

}
