<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //protected $redirectTo = '/home';

    public function username(){
      return 'username';
    }

    public function index(){
      if(Auth::viaRemember()){
        return redirect('dashboard');
      }
      return view('login');
    }

    /**
     * Overwritten method, called after the user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */

    public function authenticated(Request $request)
    {
      if(auth()->user()->status==0){
        $this->logout($request);
        return back()->withInput()->withErrors(['Your account has been locked, please contact your administrator']);
      }
     return redirect('dashboard');
    }
}
