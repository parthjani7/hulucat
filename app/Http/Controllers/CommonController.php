<?php

namespace App\Http\Controllers;


use App\Http\Requests\CreateStaffRequest;
use Spatie\Permission\Models\Role;
use Illuminate\Http\Request;
use App\Assessment;
use App\Position;
use App\User;
use App\Shop;

class CommonController extends Controller
{
    public function __construct(){
      $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard(){
      if(auth()->user()->hasRole('Manager')){
        $dashboard['staff']=auth()->user()->getUsersFromShop()->count();
        $dashboard['shops']=auth()->user()->shops->count();
      }else{
        $dashboard['staff']=User::count();
        $dashboard['shops']=Shop::count();
      }
      $dashboard['mbit']=$this->getMbitType();
      return view('pages.dashboard.index',compact('dashboard'));
    }

    public function profile(){
      $common['shoplist']=Role::where('status',1)->get();
      $common['mbittype']=$this->getMbitType();
      $profile=$this->getUsersInfo(auth()->user()->id,true)->first();
      return view('pages.profile.edit',['common'=>(object)$common,'user'=>$profile]);
    }

    public function updateProfile(CreateStaffRequest $req)
    {
      $req->rules();
      if(request('password')!='' && !\Hash::check(request('password'),$req->user()->password)){
        $req->user()->update(request()->all());
      }else{
        $req->user()->update(request()->except(['password']));
      }
      return redirect('/profile')->with('message','Profile has been updated successfully..!');
    }

    public function getShopList()
    {
      return Shop::where('status','1')->get();
    }

    public function getMbitType(){
      return ['ISTJ', 'ISFJ', 'INFJ', 'ENFJ', 'ENTJ', 'INTJ', 'ISFJ', 'ISTP', 'ISFP', 'INFP', 'INTP', 'ESTP', 'ESFP', 'ENFP', 'ENTP', 'ESTJ', 'ESFJ'];
    }

    public function getUser(User $user)
    {
      try {
        return $user;
      } catch (\Exception $e) {
        return false;
      }
    }

    public function getRole(User $user)
    {
      try {
        return $user->userRole->Role;
      } catch (\Exception $e) {
        return $e->getMessage();
      }
    }

    public function getUsersInfo($id=null,$is_admin=false){
      try {
        if($is_admin==true)
          $users=User::with('roles')->get();
        else{
          $users=User::with('roles')->whereHas('roles', function($q){
            $q->where('name', 'not like', 'admin%');
          })->get();
        }

        if($id!=null)
          $users=$users->where('id',$id);

        $users->each(function($user){
          $user->dedicated=$user->filteredShops(true)['id'];
          $user->additional=$user->filteredShops()->pluck('shop_id')->toArray();
          //$user->positions=$user->positions->pluck('id')->toArray();
          unset($user->roles);
        });

        return $users;
      } catch (\Exception $e) {
        dd($e->getMessage());
      }
    }

    protected function getUsers()
    {
      return User::all();
    }
}
