<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PaymentMethod;
use App\User;
use App\Shop;

class PaymentMethodController extends Controller
{
		private $payment_method;
		public function __construct(PaymentMethod $payment_method)
		{
			$this->payment_method=$payment_method;
		}
    public function index()
    {
			return view('admin.payment_method.list',['payment_methods'=>$this->payment_method->all()]);
    }
    public function create()
    {
			return view('admin.payment_method.create');
    }
    public function store(Request $request)
    {
			$this->validate($request,[
				'name'=>'required|string|unique:payment_methods',
				'commission'=>'required|numeric|between:0.1,99.99'
			]);
			try {
				$payment=new PaymentMethod;
				$payment->fill($request->all());
				$payment->save();
			} catch (\Exception $e) {
				\Log::error(['PaymentMethodController,store'=>$e->getMessage()]);
			}
			return redirect()->route('payment_methods.index');
    }

		public function edit(PaymentMethod $payment_method)
		{
			return view('admin.payment_method.edit',['payment_method'=>$payment_method]);
		}

		public function update(Request $request,PaymentMethod $payment_method)
		{

			$this->validate($request,[
				'name'=>'required|alpha_num|unique:payment_methods,id,'.$payment_method->id,
				'commission'=>'required|numeric|between:0.1,99.99'
			]);

			$payment_method->update($request->all());

			return redirect()
				->route('payment_methods.index');
		}

		public function showAssign()
		{
			return view('admin.payment_method.assign',[
				'payment_methods'=>$this->payment_method->where('status',1)->get(),
				'shops'=>Shop::where('status',1)->get(),
			]);
		}

		public function assign(Request $request)
		{
			try {
				\DB::table('payment_method_shop')->delete();
        if(count(request('payment_method_ids'))){
          foreach (request('payment_method_ids') as $shop => $payment_methods) {
            Shop::find($shop)->PaymentMethods()->sync($payment_methods);
          }
        }
        return back()
          ->with('message','Payment Methods has been updated successfully..!');

      } catch (Exception $e) {
        return back()
          ->with('message',$e->getMessage());
      }
		}
		public function getPaymentMethods(Shop $shop)
		{
			if($shop->PaymentMethods->count()==0){
				return \Response::json([
					'resultCode'=>'error',
					'resultValue'=>'Selected shop does not have payment method defined..!'
				]);
			}
			return \Response::json(array('resultCode'=>'success','resultValue'=>$shop->PaymentMethods));

		}

		public function toggle(PaymentMethod $payment_method)
		{
			try {
				$payment_method->update(['status'=>(!$payment_method->status)]);
				return \Response::json(array('resultCode'=>'success','status'=>$payment_method->status));
			} catch (\Exception $e) {
				echo $e->getMessage();
			}
		}
}
