<?php

namespace App\Http\Controllers;

use App\Position;
use App\User;
use Illuminate\Http\Request;

class PositionController extends CommonController
{
  public function __construct(){
    $this->middleware('role:Administrator');
  }

  public function index(){
    return view('pages.position.list',['positions'=>Position::get()]);
  }

  public function create(){
    return view('pages.position.create');
  }

  public function store(){
    $this->validate(request(),[
      'name'=>'required|min:3',
      'hourly'=>'required|min:1'
    ]);
    Position::create(request()->all());
    return redirect('/positions')->with('message','Position has been created Successfully..!');
  }

  public function edit(Position $position){
    return view('pages.position.edit',['position'=>$position]);
  }

  public function update(Position $position){
    $this->validate(request(),[
      'name'=>'required|min:3'
    ]);
    $position->update(request()->all());
    return redirect('/positions')->with('message','Position has been updated Successfully..!');
  }

  public function assignPosition(){

    return view('pages.position.assignshop',[
      'users'=>$this->getUsersInfo(),
      'positions'=>Position::where('status',1)->get(),
    ]);
  }

  public function updatePosition(Request $request){
    try {
      //Detaching all users position
      User::all()->each(function($u){ $u->positions()->detach(); });
      if($request->position_ids==null)
        return back();

      //Reassign all users based on selected positions
      foreach ($request->position_ids as $uid => $positions) {
        User::findOrFail($uid)->Positions()->sync($positions);
      }
      return back()
        ->with('message','Position has been updated successfully..!');
    } catch (\Exception $e) {
      dd($e->getMessage());
      \Log::error($e->getMessage());
    }

  }

  public function toggle(Position $position){
    try {
      if(Position::where('status','1')->count()==1 && $position->status==1)
        return \Response::json(array('resultCode'=>'error','status'=>'You must have at least one active position..!'));
      $position->update(['status'=>(!$position->status)]);
      return \Response::json(array('resultCode'=>'success','status'=>$position->status));
    } catch (Exception $e) {
      echo $e->getMessage();
    }
  }
}
