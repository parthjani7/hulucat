<?php

namespace App\Http\Controllers;

use Spatie\Permission\Models\Role;
use Illuminate\Http\Request;
use App\User;

class RoleController extends CommonController
{
    public function __construct(){
      $this->middleware('role:Administrator');
    }

    public function index(){
      return view('pages.role.list',['roles'=>Role::get()]);
    }

    public function create(){
      return view('pages.role.create');
    }

    public function store(Request $request){
      $this->validate(request(),[
        'name'=>'required|min:2'
      ]);
      $role = new Role;
      $role->name=$request->name;
      $role->status=$request->status;
      $role->save();

      return redirect('/roles')->with('message','Role has been created Successfully..!');
    }

    public function edit(Role $role){
      return view('pages.role.edit',['role'=>$role]);
    }

    public function update(Request $request,Role $role){

      $this->validate($request,[
        'name'=>'required|min:2'
      ]);
      $role->update(['name'=>$request->name,'status'=>$request->status]);
      return redirect('/roles')->with('message','Role has been updated Successfully..!');
    }

    public function showRoles(){
      return view('pages.role.grantrole',[
        'shops'=>Shop::all(),
        'roles'=>$this->getRoles(),
        'users'=>$this->getUsersInfo()
      ]);
    }

    public function grantRoles(User $user){
      try {
        $user->roles()->sync(request('role_id'));
        return \Response::make(
        [
          'resultCode'=>'Success',
          'message'=>'User Role hase been changed successfully..!'
        ]
          ,200);
      } catch (Exception $e) {
        return \Response::make(array('message'=>'Error while updating role..!'),200);
      }
    }

    public function toggle(Role $role)
    {
      try {
        $role->update(['status'=>(!$role->status)]);
        return \Response::json(array('resultCode'=>'success','status'=>$role->status));
      } catch (\Exception $e) {
        echo $e->getMessage();
      }
    }
}
