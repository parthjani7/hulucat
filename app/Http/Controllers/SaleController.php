<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SaleController extends Controller
{
    public function index()
    {
    	return view('staff.sale.index');
    }
    public function create()
    {
      try {
        return view('staff.sale.create',[
          'shops'=>auth()->user()->shops
        ]);
      } catch (\Exception $e) {
        \Log::info(['SaleController@index'=>$e]);
        return redirect('404');
      }


    }
    public function store(Request $request)
    {
      dd($request->all());
    }
}
