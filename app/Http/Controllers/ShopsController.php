<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Shop;
use App\User;
class ShopsController extends CommonController
{

    public function __construct(){
      $this->middleware('role:Administrator');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
     {
       return view('pages.shop.list',['shops'=>Shop::get()]);
     }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('pages.shop.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate(request(),[
        'name'=>'required|alpha|min:2|max:20|unique:shops'
      ]);
       Shop::create(request()->all());
      return redirect('/shops')->with('message','Shop has been created successfully..!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Shop $shop)
    {
        return view('pages.shop.edit',['shop'=>$shop]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Shop $shop)
    {
      $this->validate(request(),[
      'name'=>'required|alpha|min:2|max:20|unique:shops'
      ]);
      $shop->update($request->all());
      return redirect('shops')->with('message','Shop has been updated successfully..!');
    }

    public function assignShop(){
      try {
        return view('pages.shop.assignshop',[
          'users'=>$this->getUsersInfo(),
          'shoplist'=>Shop::where('status',1)->get(),
        ]);
      } catch (Exception $e) {
        return back()
          ->with('message',$e->getMessage());
      }
    }

    public function updateAssignShop(){
      try {

        \DB::table('shop_user')->delete();
        foreach (request('dedicated_shop') as $uid => $shop) {
          if($shop[0]==null)
            continue;
          User::find($uid)->Shops()->attach($shop[0],['is_dedicated'=>'1']);
        }

        if(count(request('shop_ids'))){
          foreach (request('shop_ids') as $uid => $shop) {
            User::find($uid)->Shops()->sync($shop,false);
          }
        }
        return back()
          ->with('message','Shop Assignments has been updated successfully..!');

      } catch (Exception $e) {
        return back()
          ->with('message',$e->getMessage());
      }
    }

    public function toggle(Shop $shop)
    {
      try {
        $shop->update(['status'=>(!$shop->status)]);
        return \Response::json(array('resultCode'=>'success','status'=>$shop->status));
      } catch (\Exception $e) {
        echo $e->getMessage();
      }
    }

}
