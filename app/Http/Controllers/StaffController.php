<?php

namespace App\Http\Controllers;

use App\Http\Requests\Staff\CreateRequest;
use App\Http\Requests\Staff\EditRequest;
use Spatie\Permission\Models\Role;
use Illuminate\Http\Request;
use App\User;
use App\Shop;

class StaffController extends CommonController
{
    public function __construct(){
      parent::__construct();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $user){

      try {
        if(auth()->user()->hasRole('Manager')){
          return view('manager.staff.list',['users'=>auth()->user()->getUsersFromShop()]);
        }
        return view('admin.staff.list',['users'=>$user->all()]);
      }catch(Exception $e){
        echo $e->getMessage();
      }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      try {
        return view('admin.staff.create',[
          'shoplist'=>Shop::all(),
          'mbittype'=>$this->getMbitType(),
          'rolelists'=>Role::where('status',1)->get(),
        ]);
      }catch(Exception $e){
        echo $e->getMessage();
      }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      try {
        $this->validate($request,[
          'email'=>'required|email|max:191|unique:users',
          'firstname'=>'required|alpha|max:30',
          'lastname'=>'required|alpha|max:30',
          'username'=>'required|max:25|unique:users',
          'password'=>'required|max:25',
          'origin_country'=>'alpha|nullable|max:30',
          'dedicated_shop'=>'required',
        ]);

        $user=new User();
        $user->fill($request->all());
        $user->password=$request->password;
        $user->save();
        $user->Shops()->sync(request('shop_ids'));
        $user->Roles()->sync(request('role_id'));
        return redirect()->route('staff.index')->with('message','Staff has been created successfully..!');
      }catch(Exception $e){
        echo $e->getMessage();
      }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $staff)
    {

      try {
        return view('admin.staff.edit',[
          'shoplist'=>Shop::all(),
          'mbittype'=>$this->getMbitType(),
          'rolelists'=>Role::where('status',1)->get(),
          'user'=>$staff,
          'additional'=>($staff->shops->pluck('id')->all()==null)?[]:$staff->shops->pluck('id')->all(),
        ]);
      } catch (Exception $e) {
        echo $e->getMessage();
      }
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(EditRequest $req,User $staff){

      $staff->update(request()->all());
      if(request('password')!='' && !\Hash::check(request('password'),$staff->password)){
        $staff->password=request('password');
        $staff->save();
      }
      if(request('role_id')!==null)
        $staff->Roles()->sync(request('role_id'));

      $staff->Shops()->sync([
        request('dedicated_shop')=>['is_dedicated'=>'1'],
      ]);
      $staff->Shops()->syncWithoutDetaching(request('shop_ids'));
      
      return redirect()
        ->route('staff.index')
        ->with('message','User has been updated successfully..!');
    }

    /**
     * Toggle the specified resource in storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function toggle(User $user)
    {
      try {
        $user->update([
          'status'=>(!$user->status)
        ]);
        return \Response::json([
          'resultCode'=>'success',
          'status'=>$user->status
        ]);
      } catch (\Exception $e) {
        \Log::error('error',[$e->getMessage()]);
      }
    }
}
