<?php

namespace App\Http\Controllers;

use App\Shop;
use App\User;
use App\Position;
use Carbon\Carbon;
use App\Timesheet;
use App\TimesheetRecord;
use Illuminate\Http\Request;

class TimesheetController extends CommonController
{
  private $shops=[];
  public function __construct(){
    $this->middleware('auth');
    $this->middleware('role:Administrator,Manager');
    if(auth()->check()){
      if(auth()->user()->hasRole('Administrator')){
        $this->shops=Shop::pluck('id');
      }else{
        $this->shops=auth()->user()->Shops()->pluck('id');
      }
    }

  }
  public function index(){
    $timesheets=Timesheet::with('shops')->whereIn('id',$this->shops)->get();
    //dd($timesheets);
    return view('pages.timesheet.list',['timesheets'=>$timesheets]);
  }

  public function create(){
    $shoplist=Shop::whereIn('id',$this->shops)
      ->where('status','1')
      ->pluck('name','id');

    $date = Carbon::now();
    $dates['today'] = $date->toDateTimeString();
    $dates['tomorrow'] = $date->addDay();

    return view('pages.timesheet.create',['shoplist'=>$shoplist,'dates'=>$dates]);
  }

  public function findStaff(Shop $shop){
    try {
      if(!$shop->users()->count())
        return \Response::make(['resultCode'=>'Warning','resultValue'=>'Selected shop does not have staff..!']);

      return \Response::make(['resultCode'=>'Success','resultValue'=>$shop->users]);
    } catch (\Exception $e) {
      \Log::info([$e->getMessage().' -> '=>$e]);
      return \Response::make(['resultCode'=>'Warning','resultValue'=>'Error fetching staff details, please try later.!']);
    }
  }

  public function findPosition(User $user){
    if($user->positions->count()>0){
      $positions=$user->positions()->where('status','1')->get();
      foreach ($positions as $position) {
        $roles[$position->id]=$position->name.' - '.$position->hourly;
      }
      return \Response::make(['resultCode'=>'Success','resultValue'=>$roles]);
    }else{
      return \Response::make(['resultCode'=>'error','resultValue'=>"Role not found..!"]);
    }
  }

  public function store(Request $request){
    dd($request->all());
    $shop_name=Shop::findOrFail($request->shop_id);
    //$position=$this->getPositionById(request('role_id'));
    $position=Position::findOrFail($request->role_id);

    $start_time=Carbon::createFromFormat('Y-m-d H:i A', request('date').' '.explode(' - ',request('date_range'))[0]);
    $end_time=Carbon::createFromFormat('Y-m-d H:i A', request('date').' '.explode(' - ',request('date_range'))[1]);

    $role_name=$position->name;
    $role_rate=$position->hourly;
    $total_minutes=$start_time->diffInMinutes($end_time)-request('brake');

    $timesheet=Timesheet::where('date',request('date'))->where('shop_id',request('shop_id'));
    if(!$timesheet->count()){
      $timesheet=Timesheet::create([
        'date'=>request('date'),
        'shop_id'=>request('shop_id'),
      ]);
    }else{
      $timesheet=$timesheet->first();
    }

    $count=$timesheet->Records()
      ->where('user_id',request('user_id'))->count();

    if($count){
      return redirect('/timesheets/edit/'.$timesheet->id)->withErrors('Timesheet already exist, Please delete first to add new timesheet..!');
    }

    $timesheet->Records()->create(array_merge(request()->all(),
      [
        'start_time' => $start_time,
        'end_time' => $end_time,
        'shop_name' => $shop_name,
        'role_name' => $role_name,
        'role_rate' => $role_rate,
        'total_minutes' => $total_minutes,
      ]
    ));
    $timesheet->Shops()->sync(request('shop_id'));

    return redirect('/timesheets/edit/'.$timesheet->id)->with('message','Timesheet Inserted Successfully..!');
  }

  public function edit(Timesheet $timesheet){

    $shoplist=$timesheet->shops()->first();
    $data=Timesheet::with('records.user')->where('id',$timesheet->id)->first();
    $data['endDate']=Date('Y-m-d',strtotime($data->date . ' +1 day'));
    return view('pages.timesheet.edit',[
      'shoplist'=>$shoplist,
      'timesheet'=>$data
    ]);
  }

  public function update(Timesheet $timesheet){

    $count=$timesheet->records()
      ->where('user_id',request('user_id'))
      ->where('role_id',request('role_id'))->count();

    if($count){
      return redirect('/timesheets/edit/'.$timesheet->id)->withErrors('Timesheet already exist, Please delete first to add new timesheet..!');
    }

    $start_time=Carbon::createFromFormat('Y-m-d H:i A', request('date').' '.explode(' - ',request('date_range'))[0]);
    $end_time=Carbon::createFromFormat('Y-m-d H:i A', request('date').' '.explode(' - ',request('date_range'))[1]);

    $shop_name=$this->getShopById(request('shop_id'))->name;
    $position=$this->getPositionById(request('role_id'));
    $role_name=$position->name;
    $role_rate=$position->hourly;
    $total_minutes=$start_time->diffInMinutes($end_time)-request('brake');

    $timesheet->Records()->create(array_merge(request()->all(),
      [
        'start_time' => $start_time,
        'end_time' => $end_time,
        'shop_name' => $shop_name,
        'role_name' => $role_name,
        'role_rate' => $role_rate,
        'total_minutes' => $total_minutes,
      ])
    );
    $timesheet->Shops()->sync(request('shop_id'));

    return redirect('/timesheets/edit/'.$timesheet->id)->with('message','Timesheet Updated Successfully..!');
  }

  public function destroyRecord(TimesheetRecord $record){
    $record->delete();
    return \Response::make(['resultCode'=>'Success']);
  }

  public function findRecord(TimesheetRecord $record){
    $user=User::find($record->user_id);
    $resultValue['user']=array($user->id=>$user->fullname);
    $positions=User::find($record->user_id)->positions()->where('status','1')->get();
    foreach ($positions as $position) {
      $resultValue['positions'][$position->id]=$position->name.' - '.$position->hourly;
    }
    return \Response::make(['resultCode'=>'Success','resultValue'=>$resultValue]);
  }

}
