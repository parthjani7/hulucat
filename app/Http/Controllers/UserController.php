<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Assessment;
use App\AssessmentData;
use App\AssessmentDataAnswer;

class UserController extends CommonController
{
  protected $my_assessments=null;
  public function __construct()
  {
    $this->middleware('auth');
    $this->middleware('role:Manager');
    if(\Auth::check())
      $this->my_assessments=auth()->user()->Assessments()->where('is_taken','0')->where('status','1')->get();
  }

		//getting List of Assessments assigned to user and not taken yet
    public function getAssessments(Request $request){
    	return view('pages.user.assessment.list',['assessments'=>$this->my_assessments]);
    }

		//show Assessment page with questions and options
		public function takeAssessment(Assessment $assessment){
			if(!in_array($assessment->id,$this->my_assessments->pluck('id')->all()))
				abort(404);

			$ques=$assessment->questions()->get();
			return view('pages.user.assessment.take',['adata'=>$ques]);
		}

		//Submit Assessment filled by user
		public function submitAssessment(Request $request,Assessment $assessment){

      if(!in_array($assessment->id,$this->my_assessments->pluck('id')->all()))
				abort(404);

      $points=0;
			foreach ($request->questions as $key => $value) {
        $is_true=($assessment->Questions()->find($key)->answer==$value)?1:0;
        $points+=$is_true;
				AssessmentDataAnswer::insert(
          [
  					'user_id'=>auth()->user()->id,
  					'assessment_data_id'=>$key,
  					'answer'=>$value,
  					'is_true'=>$is_true,
          ]
        );
			}
			auth()->user()->assessments()
				->where('id',$assessment->id)
				->update(['is_taken'=>'1','points'=>$points]);
			return redirect('/assessments/take/')->withMessage('Assessment submitted successfully..!');
		}
}
