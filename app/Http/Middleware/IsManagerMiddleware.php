<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use Closure;

class IsManagerMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      try {
        if(Auth::user()!==null && !Auth::user()->hasRole('Manager')){
          return abort('404');
        }
        return $next($request);
      }catch (\Exception $e) {
        \Log::info(['IsManagerMiddleware => '=>$e->getMessage()]);
        return redirect('/');
      }

    }
}
