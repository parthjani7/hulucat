<?php

namespace App\Http\Requests\Staff;

use Illuminate\Foundation\Http\FormRequest;

class EditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      return [
        'email'=>'required|email|max:191',
        'firstname'=>'required|alpha|max:30',
        'lastname'=>'required|alpha|max:30',
        'username'=>'required|max:25',
        'phone'=>'max:15',
        'origin_country'=>'alpha|nullable|max:30',
      ];
    }
}
