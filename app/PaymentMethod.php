<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model
{
    protected $fillable=['name','commission','include_sales','status'];

    public function Shops()
    {
      return $this->belongsToMany(Shop::class);
    }
}
