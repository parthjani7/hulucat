<?php
namespace App;
use App\Model;

class Position extends Model
{
    protected $fillable=['name','hourly','status'];

		public function users(){
			return $this->belongsToMany(User::class);
		}
}
