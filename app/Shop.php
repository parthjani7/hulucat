<?php

namespace App;

class Shop extends Model
{
    protected $fillable=['name','status'];

    public function setNameAttribute($value){
      return $this->attributes['name']=strtoupper($value);
    }

    public function Users(){
      return $this->belongsToMany(User::class);
    }
    public function Timesheet(){
      return $this->hasMany(Timesheet::class);
    }
    public function PaymentMethods()
    {
      return $this->belongsToMany(PaymentMethod::class);
    }
}
