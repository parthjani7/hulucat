<?php

namespace App;

use App\Model;
use App\TimesheetRecord;

class Timesheet extends Model
{
  protected $fillable = ['date', 'shop_id'];

  public function Records(){
    return $this->hasMany(TimesheetRecord::class);
  }
  public function Shops(){
    return $this->belongsToMany(Shop::class,'shop_timesheets');
  }

}
