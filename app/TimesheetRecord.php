<?php

namespace App;

use App\Model;

class TimesheetRecord extends Model
{

  protected $fillable = ['user_id', 'timesheet_id', 'role_id', 'shop_name', 'role_name', 'role_rate', 'start_time', 'end_time', 'total_minutes', 'break', 'remark'];

  public function Timesheet(){
    return $this->belongsTo(Timesheet::class);
  }
  public function User(){
    return $this->belongsTo(User::class);
  }

}
