<?php

namespace App;

use Illuminate\Auth\Authenticatable as TAuthenticatable;
use Illuminate\Contracts\Auth\Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class User extends Model implements Authenticatable
{
  use TAuthenticatable;
  use HasRoles;

   protected $fillable=[
   'username',
   'firstname',
   'lastname',
   'email',
   'phone',
   'origin_country',
   'address',
   'birthdate',
   'blood_type',
   'passport_num',
   'work_permit',
   'visa_type',
   'emp_date',
   'bank_name',
   'acc_number',
   'ird_number',
   'mbit_type',
   'tax_code',
   'emergency_name',
   'emergency_number',
   'emergency_rel',
   'remark',
   'status',
   'dedicated_shop'];

  protected $hidden = ['password'];

  protected $appends=['fullname'];

  public function Timesheets(){
    return $this->hasMany(Timesheet::class);
  }

  public function Shops(){
    return $this->belongsToMany(Shop::class);
  }

  public function positions(){
    return $this->belongsToMany(Position::class);
  }
  public function Assessments(){
    return $this->belongsToMany(Assessment::class)->withPivot('points','is_taken');
  }

  public function filteredShops($param=false){
    $shops=$this->belongsToMany(Shop::class)->withPivot('is_dedicated')->wherePivot('is_dedicated',$param);
    if($param==true)
      $shops=$shops->first();
    return $shops;
  }

  public function getFirstnameAttribute($val){
    return ucfirst($val);
  }
  public function getLastnameAttribute($val){
    return ucfirst($val);
  }
  public function getFullnameAttribute($val){
    return $this->firstname.' '.$this->lastname;
  }
  //Setters - Mutators
  public function setFirstnameAttribute($val){
    return $this->attributes['firstname']=ucfirst($val);
  }
  public function setLastnameAttribute($val){
    return $this->attributes['lastname']=ucfirst($val);
  }
  public function setCountryAttribute($val){
    return $this->attributes['country']=ucfirst($val);
  }
  public function setEmailAttribute($val){
    return $this->attributes['email']=strtolower($val);
  }
  public function setPasswordAttribute($val){
    return $this->attributes['password']=bcrypt($val);
  }
  public function setUsernameAttribute($val){
    return $this->attributes['username']=strtolower($val);
  }
  public function getUsersFromShop()
  {
    return $this->role(['Staff'])->get()->filter(function($u){
       return $u->shops->whereIn('id',$this->dedicated_shop)->count() > 0;
    });
  }

}
