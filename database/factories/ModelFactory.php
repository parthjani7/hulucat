<?php
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
// $factory->define(App\User::class, function (Faker\Generator $faker) {
//     static $password;
//
//     return [
//         'name' => $faker->name,
//         'email' => $faker->unique()->safeEmail,
//         'password' => $password ?: $password = bcrypt('secret'),
//         'remember_token' => str_random(10),
//     ];
// });

$factory->define(App\ProfileController::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\User::class,function (Faker\Generator $faker){
  static $password;
  return[
    'username'=>$faker->userName,
    'firstname'=>$faker->firstName,
    'lastname'=>$faker->lastName,
    'email'=>$faker->unique()->safeEmail,
    'origin_country'=>$faker->country,
    'status'=>'1',
    'password'=>'secret'
  ];
});

$factory->define(App\Shop::class,function(Faker\Generator $faker){
  return[
    'name'=>$faker->firstName,
    'status'=>'1'
  ];
});

$factory->define(App\Position::class,function(Faker\Generator $faker){
  return[
    'name'=>$faker->jobTitle,
    'hourly'=>'10.5'
  ];
});

$factory->define(
  App\Assessment::class,
  function(Faker\Generator $faker){
    return[
      'name'=>$faker->name,
      'total_questions'=>rand(1,25),
      'status'=>rand(0,1)
    ];
  }
);
