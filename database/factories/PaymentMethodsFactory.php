<?php

use Faker\Generator as Faker;

$factory->define(App\PaymentMethod::class, function (Faker $faker) {
    return [
        "name"=>$faker->company,
        "commission"=>'10',
        "include_sales"=>'0',
    ];
});
