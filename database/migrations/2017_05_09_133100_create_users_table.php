<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::defaultStringLength(191);
      Schema::create('users',function(Blueprint $table){
        $table->increments('id')->index('id');
        $table->string('username',25)->unique();
        $table->string('password',100);
        $table->rememberToken();
        $table->string('firstname',30);
        $table->string('lastname',30);
        $table->string('email')->unique();
        $table->string('phone',15)->nullable();
        $table->string('origin_country',80)->nullable();
        $table->text('address')->nullable();
        $table->date('birthdate')->nullable();
        $table->string('blood_type',3)->nullable();
        $table->string('passport_num',30)->nullable();
        $table->boolean('work_permit')->nullable();
        $table->string('visa_type',3)->nullable();
        $table->timestamp('emp_date')->nullable();
        $table->string('bank_name',60)->nullable();
        $table->string('acc_number',35)->nullable();
        $table->string('ird_number',30)->nullable();
        $table->string('mbit_type',5)->nullable();
        $table->string('tax_code',30)->nullable();
        $table->string('emergency_name',30)->nullable();
        $table->string('emergency_number',15)->nullable();
        $table->string('emergency_rel',20)->nullable();
        $table->text('remark')->nullable();
        $table->unsignedInteger('dedicated_shop')->on('shop')->references('id')->nullable();
        $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        $table->boolean('status')->default(1);
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
