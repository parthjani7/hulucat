<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTimesheets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('timesheets', function (Blueprint $table) {
        $table->increments('id');
        $table->date('date');
        $table->unsignedInteger('shop_id');
        $table->timestamps();
        $table->foreign('shop_id')->references('id')->on('shops');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('timesheets');
    }
}
