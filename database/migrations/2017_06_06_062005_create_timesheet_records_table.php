<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimesheetRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('timesheet_records', function (Blueprint $table) {
        $table->increments('id');
        $table->unsignedInteger('timesheet_id');
        $table->unsignedInteger('user_id');
        $table->unsignedInteger('role_id');
        $table->string('shop_name',30);
        $table->string('role_name',30);
        $table->string('role_rate',5)->nullable();
        $table->dateTime('start_time');
        $table->dateTime('end_time');
        $table->smallInteger('total_minutes');
        $table->smallInteger('break');
        $table->text('remark')->nullable();
        $table->timestamps();
        $table->foreign('user_id')->references('id')->on('users');
        $table->foreign('timesheet_id')->references('id')->on('timesheets');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('timesheet_records');
    }
}
