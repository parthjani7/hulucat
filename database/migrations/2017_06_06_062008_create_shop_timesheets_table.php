<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopTimesheetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('shop_timesheets', function (Blueprint $table) {
        $table->unsignedInteger('shop_id');
        $table->unsignedInteger('timesheet_id');
        $table->primary(['timesheet_id','shop_id']);
        $table->foreign('shop_id')->references('id')->on('shops');
        $table->foreign('timesheet_id')->references('id')->on('timesheets');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_timesheets');
    }
}
