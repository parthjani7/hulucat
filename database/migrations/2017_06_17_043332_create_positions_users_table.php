<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePositionsUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('position_user', function (Blueprint $table) {
        $table->unsignedInteger('user_id');
        $table->unsignedInteger('position_id');
        $table->foreign('user_id')->references('id')->on('users');
        $table->foreign('position_id')->references('id')->on('positions');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('position_user');
    }
}
