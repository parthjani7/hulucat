<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssessmentDataAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assessment_data_answers', function (Blueprint $table) {
          $table->unsignedInteger('user_id');
          $table->unsignedInteger('assessment_data_id');
          $table->text('answer');
          $table->boolean('is_true');
          $table->foreign('assessment_data_id')->references('id')->on('assessment_data')->onDelete('cascade');
          $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assessment_data_answers');
    }
}
