<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePaymentMethodShop extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_method_shop', function (Blueprint $table) {
          $table->unsignedInteger('payment_method_id')
            ->on('payment_methods')->foreign('id');
          $table->unsignedInteger('shop_id')
            ->on('payment_methods')->foreign('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_method_shop');
    }
}
