<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
      //  $this->call(UsersDataTableSeeder::class);
        //$this->call(UserRoleTableSeeder::class);
        $this->call(ShopsTableSeeder::class);
        $this->call(PaymentMethodsTableSeeder::class);
        // $this->call(PositionTableSeeder::class);
        // $this->call(AssessmentTableSeeder::class);
    }
}
