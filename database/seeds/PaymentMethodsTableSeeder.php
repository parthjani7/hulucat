<?php

use Illuminate\Database\Seeder;

class PaymentMethodsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      try {
        factory(App\PaymentMethod::class,5)->create();
      } catch (\Exception $e) {
        \Log::error([$e->getMessage()]);
      }
    }
}
