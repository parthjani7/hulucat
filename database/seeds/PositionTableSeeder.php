<?php

use Illuminate\Database\Seeder;

class PositionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      try {
        factory(App\Position::class,3)->create();
      } catch (Exception $e) {
        dd($e->getMessage());
      }
    }
}
