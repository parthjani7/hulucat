<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Query\Builder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     // this fuction won't call, unless registreed in DataBaseSeeder
    public function run()
    {
      try {
        $u= new App\User;
        $u->username='parthjani7';
        $u->password='parth';
        $u->firstname='Parth';
        $u->lastname='Jani';
        $u->email='parthjani4@gmail.com';
        $u->save();
        $u->assignRole('Administrator');
        factory(App\User::class, 5)->create()
        ->each(function($u){
          //$u->Roles()->attach('2');
          $u->assignRole('Staff');
        });
      } catch (Exception $e) {
        dd($e->getMessage());
      }
    }
}
