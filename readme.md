# Project Title

HuluCat is a web based Point of Sale system, developed using Laravel - 5.6 as a Backend Framework.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

To Install it on your local machine, makes sure your local machine have :

* [Composer](http://php.net/downloads.php) - PHP >= 7.1
* [PHP](https://maven.apache.org/)
* [NPM](https://www.npmjs.com/package/download)

### Installing

To Install HuluCat, Run Following Command

```
composer install
```

To Run Migration:

```
php artisan migrate
```

Default Username and Password for Administrator

```
Default Username : parthjani7
Default Password: parth
```

## Authors

* **Parth Jani** - *Fullstack Application Developer* - [Portfolio](https://parthjani.in/)

## Acknowledgments

* [Abdullah Almsaeed - AdminLTE](https://github.com/almasaeed2010/AdminLTE/releases)
* [Laravel](http://laravel.com/)
* [Spatie](https://spatie.be/en/opensource/laravel)