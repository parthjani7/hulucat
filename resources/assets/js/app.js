
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

//window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('example', require('./components/Example.vue'));
//
// const app = new Vue({
//     el: '#app'
// });

$('a.toggleStatus').click(function(e){
	if(!confirm('Are you sure you want to toggle this status ?')){
		return false;
	}
	var label=$(this);
	$.ajax({
		headers: {
			'X-CSRF-TOKEN': $('input[name="_token"]').val()
		},
		url:label.attr('href'),
		method:'post',
		success:function(res){
			if(res.resultCode=='success'){
				toggleLabel(label.find('span.label'),res.status);
			}
		}
	});
	e.preventDefault();
})

function toggleLabel(label,val){
	console.info(val);
	if(val==false){
		label.removeClass('bg-green');
		label.addClass('bg-red');
		label.html('Inactive')
	}else{
		label.removeClass('bg-red');
		label.addClass('bg-green');
		label.html('Active')
	}
}
