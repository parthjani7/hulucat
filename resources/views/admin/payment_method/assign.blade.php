@extends('master')
@section('title','Payment Methods Assignments')
@section('pagename','Assign Payment Methods')
@section('contents')
<section class="content-header">
  <h1>@yield('pagename')</h1>
</section>
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      @include('pages.partials.success')
      <div class="box box-success">
        <form method="post">
          <input type="submit" style='margin:10px;' class='btn btn-success pull-right' value="Update Assignments">
          <div class="box-body">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th width='1'>#</th>
                  <th>Shop Name</th>
                  <th>Payment Methods</th>
                </tr>
              </thead>
              <tbody>
                @if($shops->count()==0)
                  <tr><td colspan='5' align='center'><h4>Records not Found</h4></td></tr>
                @else
                  @foreach ($shops as $shop)
                    <tr>
                      <td>{{$loop->iteration}}</td>
                      <td>{{$shop->name}}</td>
                      <td>
                        @foreach ($shop->PaymentMethods as $payment_method)
                          <label class="checkbox-inline">
                            <input type="checkbox"
                              {{in_array($payment_method->id,$shop->PaymentMethods->pluck('id')->toArray())?'checked':''}}
                            class='additional' name="payment_method_ids[{{$shop->id}}][]" value="{{$payment_method->id}}">{{$payment_method->name}}
                          </label>
                        @endforeach
                        @foreach ($payment_methods->whereNotIn('id',$shop->PaymentMethods->pluck('id')) as $payment_method)
                          <label class="checkbox-inline">
                            <input type="checkbox"
                              {{in_array($payment_method->id,$shop->PaymentMethods->pluck('id')->toArray())?'checked':''}}
                            class='additional' name="payment_method_ids[{{$shop->id}}][]" value="{{$payment_method->id}}">{{$payment_method->name}}
                          </label>
                        @endforeach
                      </td>
                    </tr>
                  @endforeach
                @endif
              </tbody>
            </table>
            {{csrf_field()}}
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
@endsection
@section('footerscript')
<script type="text/javascript">
$(document).ready(function(){
  $('.status_confirm').click(function(e){

  });
  $('a.ajax').click(function(e){
    if(!confirm('Are you sure you want to change the user\'s status ?')){
      return false;
    }
    var label=$(this);
    $.ajax({
      headers: {
        'X-CSRF-TOKEN': $('input[name="_token"]').val()
      },
      url:label.attr('href'),
      method:'post',
      success:function(res){
        if(res.resultCode=='success'){
          toggleLabel(label.find('span.label'),res.status);
        }
      }
    });
    e.preventDefault();
  })
  function toggleLabel(label,val){
    console.info(val);
    if(val==false){
      label.removeClass('bg-green');
      label.addClass('bg-red');
      label.html('Inactive')
    }else{
      label.removeClass('bg-red');
      label.addClass('bg-green');
      label.html('Active')
    }
  }

});
</script>
@endsection
