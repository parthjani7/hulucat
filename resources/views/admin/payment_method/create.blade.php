@extends('master')
@section('title','Create Payment Method')
@section('pagename','Add Payment Method')
@section('contents')
<style media="screen">
  /*.checkbox-inline+.checkbox-inline{margin-left:0px;}
  .checkbox-inline{width: 115px;}*/
</style>
<section class="content-header">
  <h1>@yield('pagename')</h1>
</section>
<!-- Main content -->
<section class="content">
  @include('pages.partials.errors')

  <form role="form" autocomplete="off" action='{{route("payment_methods.store")}}' method='post'>
    <div class="row">
      <div class="col-md-12">
        <div class="box box-success">
          <div class="box-header with-border">
            <h3 class="box-title">Fill Details</h3>
          </div>
          <div class="box-body">
            <div class="form-group">
              <label>Name</label>
              <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-credit-card"></i></div>
                <input required name="name" placeholder='Enter Payment Method' class="form-control" value="{{old('name')}}" type="text">
              </div>
            </div>
            <div class="form-group">
              <label>Commission</label>
              <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-percent"></i></div>
                <input required name="commission" placeholder='Commission (in %)' class="form-control" value="{{old('commission')}}" type="text">
              </div>
            </div>
            <div class="form-group">
              <label>Status : </label>&nbsp;
              <label>
                <input type="radio" required name="status" value='1' class="minimal" checked>
                Active
              </label>
              &nbsp;
              <label>
                <input type="radio" required name="status" class="minimal" value='0'>
                Inactive
              </label>
            </div>
            <button type='submit' class="btn btn-success btn-md submit">Add Method</button>
          </div>
        </div>
      </div>
    </div>
    {{csrf_field()}}
  </form>
</section>
@endsection
@section('footerscript')
<script>
$(document).ready(function(){
  $('.datepicker').datepicker({autoclose: true,format:'yyyy-mm-dd'});
  $('#dedicated_shop').change(function(){hideShop($(this).val())});
  $('form')[0].reset();
})

function hideShop(shop){
  $('.additional').closest('label').removeClass('hidden');
  $('#additional'+shop).removeAttr('checked');
  $('#additional'+shop).closest('label').addClass('hidden');
}
</script>
@endsection
