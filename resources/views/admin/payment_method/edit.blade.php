@extends('master')
@section('title','Edit Payment Method')
@section('pagename','Payment Method Information')
@section('contents')
<section class="content-header">
  <h1>@yield('pagename')</h1>
</section>
<!-- Main content -->
<section class="content">
    @include('pages.partials.errors')
  <form role="form" action='{{route('payment_methods.update',$payment_method->id)}}' method='post'>
    <div class="row">
      <div class="col-md-12">
        <div class="box box-success">
          <div class="box-header with-border">
            <h3 class="box-title">Fill Details</h3>
          </div>
          <div class="box-body">
            <div class="form-group">
              <label>Name</label>
              <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-credit-card"></i></div>
                <input required name="name" placeholder='Enter Payment Method' class="form-control" value="{{old('name')??$payment_method->name}}" type="text">
              </div>
            </div>
            <div class="form-group">
              <label>Commission</label>
              <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-percent"></i></div>
                <input required name="commission" placeholder='Commission (in %)' class="form-control" value="{{old('commission')??$payment_method->commission}}" type="text">
              </div>
            </div>
            <div class="form-group">
              <label>Status : </label>&nbsp;
              <label>
                <input type="radio" {{($payment_method->status=='1')?'checked':''}} required name="status" value='1' class="minimal"> Active
              </label>
              &nbsp;
              <label>
                <input type="radio" {{($payment_method->status=='0')?'checked':''}} required name="status" class="minimal" value='0'> Inactive
              </label>
            </div>
            <button type='submit' class="btn btn-success btn-md submit">Update</button>
          </div>
        </div>
      </div>
    </div>
    {{ csrf_field() }}
    {{ method_field('PATCH') }}
  </form>
</section>
@endsection
