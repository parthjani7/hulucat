@extends('master')
@section('title','Payent Methods List')
@section('pagename','Payent Methods')
@section('contents')
<section class="content-header">
  <h1>@yield('pagename')</h1>
</section>
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      @include('pages.partials.success')
      <div class="box box-success">
        <div class="box-header">
          <a href='{{route('payment_methods.create')}}' class='btn btn-success'><i class="fa fa-user-plus"></i> Add More</a>
          <!-- <h3 class="box-title">Hover Data Table</h3> -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <!-- @if(isset($status))
            <div class="alert alert-success">
              {{$status}}
            </div>
          @endif -->
          <table id="stafflist" class="table table-bordered table-hover">
            <thead>
              <tr>
                <th>#</th>
                <th>Name</th>
                <th>Commision</th>
                <th>Status</th>
                <th class="text-center">Action</th>
              </tr>
            </thead>
            <tbody>
              @if($payment_methods->count()==0)
                <tr><td colspan='7' align='center'><h4>Records not Found</h4></td></tr>
              @else
                @foreach ($payment_methods as $payment_method)
                  <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$payment_method->name}}</td>
                    <td>{{$payment_method->commission}}</td>
                    <td>
                      <a href='{{route("payment_method_toggle",$payment_method->id)}}' class='toggleStatus' title='click to toggle'>
                        <span class="label bg-{{($payment_method->status=='1')?'green':'red'}}">
                          {{($payment_method->status=='1')?'Active':'Inactive'}}
                        </span>
                      </a>
                    </td>
                    <td align='center'><a title='Edit' class='purple' href="{{route('payment_methods.edit',$payment_method->id)}}"><i class='fa fa-pencil'></i></td>
                  </tr>
                @endforeach
              @endif
            </tbody>
          </table>
          {{csrf_field()}}
        </div>
        <!-- /.box-body -->
      </div>
    </div>
  </div>
</section>
@endsection
@section('footerscript')
<script type="text/javascript">
// $(document).ready(function(){
//
// });
</script>
@endsection
