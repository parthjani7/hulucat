@extends('master')
@section('title','Staff List')
@section('pagename','Staff')
@section('contents')
<section class="content-header">
  <h1>@yield('pagename')</h1>
</section>
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      @include('pages.partials.success')
      <div class="box box-success">
        <div class="box-header">
          <a href='/staff/create' class='btn btn-success'><i class="fa fa-user-plus"></i> Add More</a>
          <!-- <h3 class="box-title">Hover Data Table</h3> -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <!-- @if(isset($status))
            <div class="alert alert-success">
              {{$status}}
            </div>
          @endif -->
          <table id="stafflist" class="table table-bordered table-hover">
            <thead>
              <tr>
                <th>#</th>
                <th>Full Name</th>
                <th>Email</th>
                <th>Status</th>
                <th>Role</th>
                <th>Creation Date</th>
                <th class="text-center">Action</th>
              </tr>
            </thead>
            <tbody>
              @if($users->count()==0)
                <tr><td colspan='7' align='center'><h4>Records not Found</h4></td></tr>
              @else
                @foreach ($users as $user)
                  <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$user->fullname}}</td>
                    <td>{{$user->email}}</td>
                    <td>
                      <a href='{{route("staff_toggle",$user->id)}}' class='toggleStatus' title='click to toggle'>
                        <span class="label bg-{{($user->status=='1')?'green':'red'}}">
                          {{($user->status=='1')?'Active':'Inactive'}}
                        </span>
                      </a>
                    </td>
                    <td>{{$user->Roles()->first()->name}}</td>
                    <td>{{(isset($user->created_at))?$user->created_at->toDateString():''}}</td>
                    <td align='center'><a title='Edit' class='purple' href="{{route('staff.edit',$user->id)}}"><i class='fa fa-pencil'></i></td>
                  </tr>
                @endforeach
              @endif
            </tbody>
          </table>
          {{csrf_field()}}
        </div>
        <!-- /.box-body -->
      </div>
    </div>
  </div>
</section>
@endsection
@section('footerscript')
<script type="text/javascript">
$(document).ready(function(){
  $('.status_confirm').click(function(e){

  });

});
</script>
@endsection
