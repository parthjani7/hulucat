@extends('master')
@section('title','Create Staff')
@section('pagename','Add Staff')
@section('contents')
<style media="screen">
  /*.checkbox-inline+.checkbox-inline{margin-left:0px;}
  .checkbox-inline{width: 115px;}*/
</style>
<section class="content-header">
  <h1>@yield('pagename')</h1>
</section>
<!-- Main content -->
<section class="content">
  @include('pages.partials.errors')

  <form role="form" autocomplete="off" action='{{route("staff.store")}}' method='post'>
    <div class="row">
      <div class="col-md-6">
        <div class="box box-success">
          <div class="box-header with-border">
            <h3 class="box-title">Login</h3>
          </div>
          <div class="box-body">
            <div class="form-group">
              <label>Username</label>
              <input required id="username" name="username" placeholder='Enter Username' class="form-control" value="{{old('username')}}" type="text">
            </div>
            <div class="form-group">
              <label>Email Address</label>
              <input required id="email" name="email" placeholder='Enter Email' class="form-control" value="{{old('email')}}" type="email">
            </div>
            <div class="form-group">
              <label>Enter Password</label>
              <input type='password' name="password" class="form-control" placeholder='Password' value="{{old('password')}}" required id="password" requird>
            </div>
          </div>
        </div>
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Personal Information</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <div class="box-body">
            <div class="form-group">
              <label>First Name</label>
              <input name="firstname" class="text form-control small" value="{{old('firstname')}}" required id="firstname" placeholder='Enter Firstname' type="text">
            </div>
            <div class="form-group">
              <label>Last Name</label>
              <input name="lastname" placeholder='Enter Lastname' class="text form-control small" value="{{old('lastname')}}" required id="lastname" type="text">
            </div>
            <div class="form-group">
              <label>Date of Birth</label>
              <div class="input-group date">
                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                <input value="{{old('birthdate')}}" id="birthdate" name="birthdate" placeholder='Select DOB' readonly class="form-control pull-right datepicker" type="text">
              </div>
            </div>
            <div class="form-group">
              <label>Blood Type</label>
              <select name="blood_type" class="form-control text" id="blood_type">
                <option value="">Select Option</option>
                <option value="A">A</option>
                <option value="B">B</option>
                <option value="AB">AB</option>
                <option value="O">O</option>
              </select>
            </div>
            <div class="form-group">
              <label>Country of Origin</label>
              <input name="origin_country" placeholder='Enter Country' class="text form-control small" value="{{old('origin_country')}}" id="origin_country" type="text">
            </div>
            <div class="form-group">
              <label>Passport Number</label>
              <input name="passport_num" placeholder='Enter Passport Number'  class="text form-control small" value="{{old('passport_num')}}" id="passport_num" type="text">
            </div>
            <div class="form-group">
              <label>Visa Type</label>
              <select name="visa_type" class="form-control text" value="{{old('visa_type')}}" id="visa_type">
                <option value="">Select Option</option>
                <option value="WV">Working Visa</option>
                <option value="SV">Student Visa</option>
                <option value="PR">PR/Citizen</option>
              </select>
            </div>
            <div class="form-group">
              <label>Work Permit</label>
              <select name="work_permit" class="form-control text" value="{{old('work_permit')}}" id="work_permit">
                <option value="">Select Option</option>
                <option value="1">Yes</option>
                <option value="0">No</option>
              </select>
            </div>
            <div class="form-group">
              <label>IRD Number</label>
              <input name="ird_number" placeholder='Enter IRD'  class="text form-control small" value="{{old('ird_number')}}" id="ird_number" type="text">
            </div>
            <div class="form-group">
              <label>Tax Code</label>
              <input name="tax_code" class="text form-control small" placeholder='Enter Tax Code' value="{{old('tax_code')}}" id="tax_code" type="text">
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title">Employment</h3>
          </div>
          <div class="box-body">
            <div class="form-group">
              <label>Employement Date</label>
              <div class="input-group date">
                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                <input value="{{old('emp_date')}}" id="emp_date" name="emp_date" placeholder='Select Date' readonly class="form-control pull-right datepicker" type="text">
              </div>
            </div>
            <div class="form-group">
              <label>Dedicated Shop</label>
              <select class="form-control" required id="dedicated_shop" name="dedicated_shop">
                <option value=''> -- SELECT DEDICATED SHOP -- </option>
                @foreach ($shoplist as $shop)
                  <option value='{{$shop->id}}'>{{$shop->name}}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <label>Additional Shop</label><br>
              <div class="row">
                @foreach ($shoplist as $shop)
                <div class="col-md-3 col-sm-4 col-xs-6">
                  <label class="checkbox-inline">
                    <input type="checkbox" class='additional' name="shop_ids[]" id="additional{{$shop->id}}" value="{{$shop->id}}">{{$shop->name}}
                  </label>
                </div>
                @endforeach
              </div>
            </div>
            @if(isset($rolelists))
            <div class="form-group">
              <label>User Type</label><br>
              @foreach ($rolelists as $role)
                <label class="radio-inline">
                  <input type="radio" required name="role_id" id="role_id" value="{{$role->id}}">{{$role->name}}
                </label>
              @endforeach
            </div>
            @endif
            <div class="form-group">
              <label>MBTI Type</label>
              <select name="mbit_type" class="form-control text" id="mbit_type">
                <option value="">Select Option</option>
                @foreach ($mbittype as $mbit)
                  <option>{{$mbit}}</option>
                @endforeach
              </select>
            </div>
          </div>
        </div>
      </div>
    </div>
    {{csrf_field()}}
    <button type='submit' name="btnAdd" class="btn btn-success btn-md submit" id="btnAdd">Add Staff</button>
  </form>
</section>
@endsection
@section('footerscript')
<script>
$(document).ready(function(){
  $('.datepicker').datepicker({autoclose: true,format:'yyyy-mm-dd'});
  $('#dedicated_shop').change(function(){hideShop($(this).val())});
  $('form')[0].reset();
})

function hideShop(shop){
  $('.additional').closest('label').removeClass('hidden');
  $('#additional'+shop).removeAttr('checked');
  $('#additional'+shop).closest('label').addClass('hidden');
}
</script>
@endsection
