@extends('master')
@section('title','Edit Staff')
@section('pagename','Staff Information')
@section('contents')
<section class="content-header">
  <h1>@yield('pagename')</h1>
</section>
<!-- Main content -->
<section class="content">
  @include('pages.partials.errors')
  <form role="form" action='{{route("staff.update",$user->id)}}' method='post'>
    <div class="row">
      <div class="col-md-6">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Login</h3>
          </div>
          <div class="box-body">
            <div class="form-group">
              <label>Username</label>
              <input required id="username" name="username" placeholder='Enter Username' class="form-control" value='{{$user->username}}' type="text">
            </div>
            <div class="form-group">
              <label>Email Address</label>
              <input required id="email" name="email" placeholder='Enter Email'  class="form-control" value='{{$user->email}}' type="email">
            </div>
            <div class="form-group">
              <label>Enter Password</label>
              <input type='password' name="password" class="form-control" placeholder='Password' id="password" >
            </div>
          </div>
        </div>
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Personal Information</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <div class="box-body">
            <div class="form-group">
              <label>First Name</label>
              <input required name="firstname" class="text form-control small"  id="firstname" value='{{$user->firstname}}' placeholder='Enter Firstname' type="text">
            </div>
            <div class="form-group">
              <label>Last Name</label>
              <input required name="lastname" placeholder='Enter Lastname'  class="text form-control small" value='{{$user->lastname}}' id="lastname" type="text">
            </div>
            <div class="form-group">
              <label>Date of Birth</label>
              <div class="input-group date">
                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                <input id="birthdate" name="birthdate" placeholder='Select DOB' readonly value='{{$user->birthdate}}' class="form-control pull-right datepicker" type="text">
              </div>
            </div>
            <div class="form-group">
              <label>Blood Type</label>
              <select name="blood_type" class="form-control text" id="blood_type">
                <option value="">Select Option</option>
                <option {{($user->blood_type=='A')?'selected':''}} value="A">A</option>
                <option {{($user->blood_type=='B')?'selected':''}} value="B">B</option>
                <option {{($user->blood_type=='AB')?'selected':''}} value="AB">AB</option>
                <option {{($user->blood_type=='O')?'selected':''}} value="O">O</option>
              </select>
            </div>

            <div class="form-group">
              <label>Country of Origin</label>
              <input name="origin_country" placeholder='Enter Country' class="text form-control small" value='{{$user->origin_country}}' id="origin_country" type="text">
            </div>
            <div class="form-group">
              <label>Passport Number</label>
              <input name="passport_num" placeholder='Enter Passport Number'  class="text form-control small" value='{{$user->passport_num}}' id="passport_num" type="text">
            </div>
            <div class="form-group">
              <label>Visa Type</label>
              <select name="visa_type" class="form-control text" id="visa_type">
                <option value="">Select Option</option>
                <option {{($user->visa_type=='WV')?'selected':''}} value="WV">Working Visa</option>
                <option {{($user->visa_type=='SV')?'selected':''}} value="SV">Student Visa</option>
                <option {{($user->visa_type=='PR')?'selected':''}} value="PR">PR/Citizen</option>
              </select>
            </div>
            <div class="form-group">
              <label>Work Permit</label>
              <select name="work_permit" class="form-control text" id="work_permit">
                <option value="">Select Option</option>
                <option {{($user->work_permit==1)?'selected':''}} value="1">Yes</option>
                <option {{($user->work_permit==0)?'selected':''}} value="0">No</option>
              </select>
            </div>
            <div class="form-group">
              <label>IRD Number</label>
              <input name="ird_number" placeholder='Enter IRD'  class="text form-control small" value='{{$user->ird_number}}' id="ird_number" type="text">
            </div>
            <div class="form-group">
              <label>Tax Code</label>
              <input name="tax_code" class="text form-control small" placeholder='Enter Tax Code' value='{{$user->tax_code}}' id="tax_code" type="text">
            </div>
            <div class="form-group">
              <button type='submit' name="btnAdd" class="btn btn-primary btn-md" id="btnUpdate">Update Staff</button>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Employment</h3>
          </div>
          <div class="box-body">
            <div class="form-group">
              <label>Employement Date</label>
              <div class="input-group date">
                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                <input id="emp_date" value='{{explode(' ',$user->emp_date)[0]}}' name="emp_date" placeholder='Select Date' readonly class="form-control pull-right datepicker" type="text">
              </div>
            </div>
            <div class="form-group">
              <label>Dedicated Shop</label>
              <select class="form-control" required id="dedicated_shop" name="dedicated_shop">
                <option value=''> -- SELECT DEDICATED SHOP -- </option>
                @foreach ($shoplist as $shop)
                  <option {{ ($shop->id==$user->dedicated_shop)?'selected':''}} value='{{$shop->id}}'>{{$shop->name}}</option>
                @endforeach
              </select>
            </div>

            <div class="form-group">
              <label>Additional Shop</label><br>
              @foreach ($shoplist as $shop)
                <label class="checkbox-inline {{ ($shop->id==$user->dedicated_shop)?'hidden':''}}" >
                  <input {{ (in_array($shop->id,$additional))?'checked':''}} type="checkbox" class='additional' name="shop_ids[]" id="additional{{$shop->id}}" value="{{$shop->id}}">{{$shop->name}}
                </label>
              @endforeach
            </div>
            @if(isset($rolelists))
            <div class="form-group">
              <label>User Type</label><br>
              @foreach ($rolelists as $role)
                <label class="radio-inline">
                  <input type="radio" name="role_id" id="role_id" {{($user->Roles->first()->id==$role->id)?'checked':''}} value="{{$role->id}}">{{$role->name}}
                </label>
              @endforeach
            </div>
            @endif

            <div class="form-group">
              <label>MBTI Type</label>
              <select name="mbit_type" class="form-control text" id="mbit_type">
                <option value="">Select Option</option>
                @foreach ($mbittype as $mbit)
                  <option>{{$mbit}}</option>
                @endforeach
              </select>
            </div>
          </div>
        </div>
      </div>
    </div>
    {{ csrf_field() }}
    {{method_field('PATCH')}}
  </form>
</section>
@endsection
@section('footerscript')
<script>
$(document).ready(function(){
  $('.datepicker').datepicker({autoclose: true,format:'yyyy-mm-dd'});
  $('#dedicated_shop').change(function(){hideShop($(this).val())});
})

function hideShop(shop){
  console.info(shop);
  $('.additional').closest('label').removeClass('hidden');
  $('#additional'+shop).removeAttr('checked');
  $('#additional'+shop).closest('label').addClass('hidden');
}
</script>
@endsection
