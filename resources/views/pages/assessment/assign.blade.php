@extends('master')
@section('title','Assessments Assignments')
@section('pagename','Assign Assessments')
@section('contents')
<section class="content-header">
  <h1>@yield('pagename')</h1>
</section>
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      @include('pages.partials.success')
      <div class="box box-success">
        <form method="post">
          <input type="submit" style='margin:10px;' class='btn btn-success pull-right' value="Update Assignments">
          <div class="box-body">
            <table id="stafflist" class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th width='1'>#</th>
                  <th>Full Name</th>
                  <th>Email</th>
                  <th width='1'>Role</th>
                  <th>Assessments</th>
                </tr>
              </thead>
              <tbody>
                @if($users->count()==0)
                  <tr><td colspan='5' align='center'><h4>Records not Found</h4></td></tr>
                @else
                  @foreach ($users as $user)
                    <tr>
                      <td>{{$loop->iteration}}</td>
                      <td>{{$user->firstname}} {{$user->lastname}}</td>
                      <td>{{$user->email}}</td>
                      <td>{{$user->Roles->first()->name}}</td>
                      <td>

                        <?php $assessment_arr = $user->Assessments()->pluck('id')->all(); ?>

                        @foreach ($assessmentlist as $assessment)
                          <label class="checkbox-inline">
                            <input type="checkbox" class='additional'
                             name="assessment_ids[{{$user->id}}][]"
                             {{in_array($assessment->id,$assessment_arr)?'checked':''}} value="{{$assessment->id}}">{{$assessment->name}}
                          </label>
                        @endforeach
                      </td>
                    </tr>
                  @endforeach
                @endif
              </tbody>
            </table>
            {{csrf_field()}}
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
@endsection
@section('footerscript')

@endsection
