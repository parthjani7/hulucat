@extends('master')
@section('title','Create Assessment')
@section('pagename','Add Assessment')
@section('headerscript')
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="/plugins/iCheck/all.css">
@endsection
@section('contents')
<section class="content-header">
  <h1>@yield('pagename')</h1>
</section>
<!-- Main content -->
<section class="content">

  @include('pages.partials.errors')
  @include('pages.partials.success')

  <form role="form" action='/assessments/create' method='post'>
    <div class="row">
      <div class="col-md-12">
        <div class="box box-info">
          <div class="box-body">
            <div class="form-group">
              <label>Assessment Name</label>
              <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-desktop"></i></div>
                <input required name="name" placeholder='Enter Assessment Name' class="form-control" value="{{old('name')}}" type="text">
              </div>
            </div>
            <div class="form-group">
              <label> Status : </label>&nbsp;
              <label>
                <input type="radio" required name="status" value='1' class="minimal" checked>
                Active
              </label>
              <label>
                <input type="radio" required name="status" class="minimal" value='0'>
                Inactive
              </label>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title">Add Questions</h3>
          </div>
          <div class="box-body">
            <div class="form-group">
              <div id="question_set">
                <div class="question">
                  <div class="input-group">
                    <div class="input-group-addon que_num">1</div>
                    <input id="question1" required name="data[0][que][]" placeholder='Enter Question' class="form-control" value="{{old('question1')}}" type="text">
                  </div>
                  <br>
                  <div class="form-group">
                    <label> Question Type : </label>&nbsp;
                    <label>
                      <input type="radio" required name="data[0][que_type][]" value='0' class="que_option que_type" checked> MCQ
                    </label> &nbsp;
                    <label>
                      <input type="radio" required name="data[0][que_type][]" class="que_option que_type" value='1'> Blanks
                    </label>
                  </div>
                  <div class="form-group mcq">
                    <div class="row">
                      <div class="col-md-3">
                        <div class="input-group">
                          <div class="input-group-addon"><input type="radio" name="answers[0]" class='radio_answer' required="required" title="select correct answer" value='0'></div>
                          <input required name="data[0][option][]" placeholder='Option 1' class="form-control option_ans" value="{{old('option[1]')}}" type="text">
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="input-group">
                          <div class="input-group-addon">
                            <input type="radio" name="answers[0]" class='radio_answer' required="required" title="select correct answer" value='1'>
                          </div>
                          <input required name="data[0][option][]" placeholder='Option 2' class="form-control option_ans" value="{{old('option[2]')}}" type="text">
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="input-group">
                          <div class="input-group-addon">
                            <input type="radio" name="answers[0]" class='radio_answer' required="required" title="select correct answer" value='2'>
                          </div>
                          <input required name="data[0][option][]" placeholder='Option 3' class="form-control option_ans" value="{{old('option[3]')}}" type="text">
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="input-group">
                          <div class="input-group-addon">
                            <input type="radio" name="answers[0]" class='radio_answer' required="required" title="select correct answer" value='3'>
                          </div>
                          <input required name="data[0][option][]" placeholder='Option 4' class="form-control option_ans" value="{{old('option[4]')}}" type="text">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row blanks hidden">
                    <div class="col-sm-12">
                      <div class="form-group">
                        <input name="data[0][answer][]" placeholder='Answer' class="form-control blank_answer" value="{{old('answer[1]')}}" type="text">
                      </div>
                    </div>
                  </div>
                </div>
                <button type='button' id="btnAddQue" class="btn btn-primary btn-md"><i class='fa fa-plus'></i> Add Question</button>
                <button type='submit' name="btnAdd" class="btn btn-success btn-md submit" id="btnAdd">Submit Assessment</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    {{csrf_field()}}
  </form>
</section>
@endsection
@section('footerscript')
<script src="/plugins/iCheck/icheck.min.js"></script>
<script type="text/javascript">

$(document).ready(function(){
  var counter=0;
  $('#btnAddQue').click(function(){
    counter++;
    var new_fields='<div class="question" id="que'+counter+'"> <hr> <div class="row"> <div class="col-sm-9"> <div class="input-group"> <div class="input-group-addon que_num">'+(counter+1)+'</div><input id="question1" required name="data['+counter+'][que][]" placeholder="Enter Question" class="form-control" type="text"> </div></div><div class="col-sm-3"> <div class="form-group"> <button type="button" name="btnDel" onclick="initDel('+counter+')" class="btn btn-danger btn-block">Remove</button> </div></div></div><br><div class="form-group"> <label> Question Type : </label>&nbsp; <label> <input type="radio" required name="data['+counter+'][que_type][]" value="0" class="que_option que_type" checked> MCQ </label> &nbsp; <label> <input type="radio" required name="data['+counter+'][que_type][]" class="que_option que_type" value="1"> Blanks </label> </div><div class="form-group mcq"> <div class="row"> <div class="col-md-3"> <div class="input-group"> <div class="input-group-addon"> <input type="radio" name="answers['+counter+']" class="radio_answer" required="required" title="select correct answer" value="0"> </div><input id="option1_1" required name="data['+counter+'][option][]" placeholder="Option 1" class="form-control option_ans" type="text"> </div></div><div class="col-md-3"> <div class="input-group"> <div class="input-group-addon"> <input type="radio" name="answers['+counter+']" class="radio_answer" required="required" title="select correct answer" value="1"> </div><input id="option1_2" required name="data['+counter+'][option][]" placeholder="Option 2" class="form-control option_ans" type="text"> </div></div><div class="col-md-3"> <div class="input-group"> <div class="input-group-addon"> <input type="radio" name="answers['+counter+']" class="radio_answer" required="required" title="select correct answer" value="2"> </div><input id="option1_3" required name="data['+counter+'][option][]" placeholder="Option 3" class="form-control option_ans" type="text"> </div></div><div class="col-md-3"> <div class="input-group"> <div class="input-group-addon"> <input type="radio" name="answers['+counter+']" class="radio_answer" required="required" title="select correct answer" value="3"> </div><input id="option1_4" required name="data['+counter+'][option][]" placeholder="Option 4" class="form-control option_ans" type="text"> </div></div></div></div><div class="row blanks hidden"> <div class="col-sm-12"> <div class="form-group"> <input id="answer1" name="data['+counter+'][answer][]" placeholder="Answer" class="form-control blank_answer" type="text"> </div></div></div></div>';
    $('#question_set .question').last().after(new_fields);
    initQueRadio();
  });
});
function initDel(arg){
  if(!confirm('Are you sure you want to delete this question ?')){
    return false;
  }else {
    if($('#que'+arg).length)
      $('#que'+arg).remove();
  }
}
function initQueRadio(){
  $('.que_option').change(function(){
    var _question=$(this).closest('.question');
    if($(this).val()=='1'){
      _question.find('.mcq').addClass('hidden');
      _question.find('.option_ans').removeAttr('required');
      _question.find('.radio_answer').removeAttr('required');
      _question.find('.blanks').removeClass('hidden');
      _question.find('.blank_answer').attr('required','required');
    }else{
      _question.find('.mcq').removeClass('hidden');
      _question.find('.option_ans').attr('required','required');
      _question.find('.radio_answer').attr('required','required');
      _question.find('.blanks').addClass('hidden');
      _question.find('.blank_answer').removeAttr('required');
    }
  })
}initQueRadio();
</script>
@endsection
