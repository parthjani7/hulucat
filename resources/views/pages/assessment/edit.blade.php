@extends('master')
@section('title','Update Assessment')
@section('pagename','Edit Assessment')
@section('headerscript')
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="/plugins/iCheck/all.css">
@endsection
@section('contents')
<section class="content-header">
  <h1>@yield('pagename')</h1>
</section>
<!-- Main content -->
<section class="content">

  @include('pages.partials.errors')
  @include('pages.partials.success')

  <form role="form" method='post'>
    {{method_field('PATCH')}}
    <div class="row">
      <div class="col-md-12">
        <div class="box box-info">
          <div class="box-body">
            <div class="form-group">
              <label>Assessment Name</label>
              <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-desktop"></i></div>
                <input required name="name" placeholder='Enter Assessment Name' class="form-control" value="{{$assessment->name}}" type="text">
              </div>
            </div>
            <div class="form-group">
              <label> Status : </label>&nbsp;
              <label>
                <input type="radio" required name="status"  {{($assessment->status=='1')?'checked':''}} value='1' class="minimal">
                Active
              </label>
              <label>
                <input type="radio" {{($assessment->status=='0')?'checked':''}}  required name="status" class="minimal" value='0'>
                Inactive
              </label>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title">Add Questions</h3>
          </div>
          <div class="box-body">
            <div class="form-group">
              <div id="question_set">
                @foreach ($questions as $que)
                  <?php $decode=json_decode($que->question);?>
                  <div class="question" id="que{{$loop->index}}">
                    <div class="row">
                      <div class="col-sm-9">
                        <div class="input-group">
                          <div class="input-group-addon que_num">{{$loop->index+1}}</div>
                          <input required name="data[{{$loop->index}}][que][]" placeholder='Enter Question' value="{{$decode->que[0]}}" class="form-control" type="text">
                        </div>
                      </div>
                      <div class="col-sm-3">
                        <div class="form-group">
                          <button type="button" name="btnDel" onclick="initDel({{$loop->index}})" class="btn btn-danger btn-block">Remove</button>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label> Question Type : </label>&nbsp;
                      <label>
                        <input type="radio" required name="data[{{$loop->index}}][que_type][]" value='0' class="que_option que_type" {{($que->question_type==0)?'checked':''}}> MCQ
                      </label> &nbsp;
                      <label>
                        <input type="radio" required name="data[{{$loop->index}}][que_type][]" class="que_option que_type" value='1' {{($que->question_type==1)?'checked':''}}> Blanks
                      </label>
                    </div>
                    <div class="form-group mcq {{($que->question_type==1)?'hidden':''}}">
                      <div class="row">
                        <div class="col-md-3">
                          <div class="input-group">
                            <div class="input-group-addon">
                              <input type="radio" name="answers[{{$loop->index}}]" class='radio_answer' {{(isset($decode->options[0]) && $decode->options[0]==$que->answer)?'checked':''}} title="select correct answer" {{($que->question_type==0)?'required':''}} value='0'>
                            </div>
                            <input {{($que->question_type==0)?'required':''}} name="data[{{$loop->index}}][option][]" placeholder='Option 1' value="{{(isset($decode->options[0]))?$decode->options[0]:''}}" class="form-control option_ans" type="text">
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="input-group">
                            <div class="input-group-addon">
                              <input type="radio" name="answers[{{$loop->index}}]" class='radio_answer' title="select correct answer" {{($que->question_type==0)?'required':''}} value='1'
                               {{(isset($decode->options[1]) && $decode->options[1]==$que->answer)?'checked':''}}>
                            </div>
                            <input {{($que->question_type==0)?'required':''}} name="data[{{$loop->index}}][option][]" placeholder='Option 2' class="form-control option_ans" type="text" value="{{(isset($decode->options[1]))?$decode->options[1]:''}}">
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="input-group">
                            <div class="input-group-addon">
                              <input type="radio" name="answers[{{$loop->index}}]" class='radio_answer' title="select correct answer" {{($que->question_type==0)?'required':''}} value='2'
                               {{(isset($decode->options[2]) && $decode->options[2]==$que->answer)?'checked':''}}>
                            </div>
                            <input {{($que->question_type==0)?'required':''}} name="data[{{$loop->index}}][option][]" placeholder='Option 3' class="form-control option_ans" type="text" value="{{(isset($decode->options[2]))?$decode->options[2]:''}}">
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="input-group">
                            <div class="input-group-addon">
                              <input type="radio" name="answers[{{$loop->index}}]" class='radio_answer' title="select correct answer" {{($que->question_type==0)?'required':''}} value='3'
                               {{(isset($decode->options[3]) && $decode->options[3]==$que->answer)?'checked':''}}>
                            </div>
                            <input {{($que->question_type==0)?'required':''}} name="data[{{$loop->index}}][option][]" placeholder='Option 4' class="form-control option_ans" type="text" value="{{(isset($decode->options[3]))?$decode->options[3]:''}}">
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row blanks {{($que->question_type==0)?'hidden':''}}">
                      <div class="col-sm-12">
                        <div class="form-group">
                          <input name="data[{{$loop->index}}][answer][]" placeholder='Answer' {{($que->question_type==1)?'required':''}} class="form-control blank_answer" type="text" value='{{$que->answer}}'>
                        </div>
                      </div>
                    </div>
                    <hr>
                  </div>
                  @endforeach
                <button type='button' id="btnAddQue" class="btn btn-primary btn-md"><i class='fa fa-plus'></i> Add Question</button>
                <button type='submit' name="btnAdd" class="btn btn-success btn-md submit" id="btnAdd">Submit Assessment</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    {{csrf_field()}}
  </form>
</section>
@endsection
@section('footerscript')
<script src="/plugins/iCheck/icheck.min.js"></script>
<script type="text/javascript">

$(document).ready(function(){
  var counter={{$assessment->questions->count()}};
  $('#btnAddQue').click(function(){
    var new_fields='<div class="question" id="que'+counter+'"> <div class="row"> <div class="col-sm-9"> <div class="input-group"> <div class="input-group-addon que_num">'+(counter+1)+'</div><input id="question1" required name="data['+counter+'][que][]" placeholder="Enter Question" class="form-control" type="text"> </div></div><div class="col-sm-3"> <div class="form-group"> <button type="button" name="btnDel" onclick="initDel('+counter+')" class="btn btn-danger btn-block">Remove</button> </div></div></div><div class="form-group"> <label> Question Type : </label>&nbsp; <label> <input type="radio" required name="data['+counter+'][que_type][]" value="0" class="que_option que_type" checked> MCQ </label> &nbsp; <label> <input type="radio" required name="data['+counter+'][que_type][]" class="que_option que_type" value="1"> Blanks </label> </div><div class="form-group mcq"> <div class="row"> <div class="col-md-3"> <div class="input-group"> <div class="input-group-addon"> <input type="radio" name="answers['+counter+']" class="radio_answer" required="required" title="select correct answer" value="0"> </div><input id="option1_1" required name="data['+counter+'][option][]" placeholder="Option 1" class="form-control option_ans" type="text"> </div></div><div class="col-md-3"> <div class="input-group"> <div class="input-group-addon"> <input type="radio" name="answers['+counter+']" class="radio_answer" required="required" title="select correct answer" value="1"> </div><input id="option1_2" required name="data['+counter+'][option][]" placeholder="Option 2" class="form-control option_ans" type="text"> </div></div><div class="col-md-3"> <div class="input-group"> <div class="input-group-addon"> <input type="radio" name="answers['+counter+']" class="radio_answer" required="required" title="select correct answer" value="2"> </div><input id="option1_3" required name="data['+counter+'][option][]" placeholder="Option 3" class="form-control option_ans" type="text"> </div></div><div class="col-md-3"> <div class="input-group"> <div class="input-group-addon"> <input type="radio" name="answers['+counter+']" class="radio_answer" required="required" title="select correct answer" value="3"> </div><input id="option1_4" required name="data['+counter+'][option][]" placeholder="Option 4" class="form-control option_ans" type="text"> </div></div></div></div><div class="row blanks hidden"> <div class="col-sm-12"> <div class="form-group"> <input id="answer1" name="data['+counter+'][answer][]" placeholder="Answer" class="form-control blank_answer" type="text"> </div></div></div></div><hr>';
    $('#question_set').first().before(new_fields);
    initQueRadio();
    counter++;
  });
});
function initDel(arg){
  if(!confirm('Are you sure you want to delete this question ?')){
    return false;
  }else {
    if($('#que'+arg).length)
      $('#que'+arg).remove();
  }
}
function initQueRadio(){
  $('.que_option').change(function(){
    var _question=$(this).closest('.question');
    if($(this).val()=='1'){
      _question.find('.mcq').addClass('hidden');
      _question.find('.option_ans').removeAttr('required');
      _question.find('.radio_answer').removeAttr('required');
      _question.find('.blanks').removeClass('hidden');
      _question.find('.blank_answer').attr('required','required');
    }else{
      _question.find('.mcq').removeClass('hidden');
      _question.find('.option_ans').attr('required','required');
      _question.find('.radio_answer').attr('required','required');
      _question.find('.blanks').addClass('hidden');
      _question.find('.blank_answer').removeAttr('required');
    }
  })
}initQueRadio();
</script>
@endsection
