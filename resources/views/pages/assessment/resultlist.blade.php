@extends('master')
@section('title','Assessments Resultlist')
@section('pagename','Assessments Resultlist')
@section('contents')
<section class="content-header">
  <h1>@yield('pagename')</h1>
</section>
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      @include('pages.partials.success')
      <div class="box box-success">
        <form method="post">
          <div class="box-body">
            <table id="stafflist" class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th width='1'>#</th>
                  <th>Full Name</th>
                  <th>Email</th>
                  <th width='1'>Role</th>
                  <th width='1'>Points</th>
                  <th>Assessments Taken</th>
                </tr>
              </thead>
              <tbody>

                @if($assessment->count()==0)
                  <tr><td colspan='5' align='center'><h4>Records not Found</h4></td></tr>
                @else
                  @foreach ($assessment as $user)

                    <tr>
                      <td>{{$loop->iteration}}</td>
                      <td>{{$user->firstname}} {{$user->lastname}}</td>
                      <td>{{$user->email}}</td>
                      <td>{{$user->Roles->first()->name}}</td>
                      <td>{{$user->assessments->first()->pivot->points}}</td>

                      <td>
                        <?php
													$assessmentlist = $user->Assessments()->where('is_taken','1')->get();
													$assessment_arr = $assessmentlist->pluck('id')->all();
												?>
												<label>
													@if($assessmentlist->count())
	                        	@foreach ($assessmentlist as $assessment)
	                            <a href="javascript:void(0)" class='result' data-href='/assessments/result/{{$user->id}}/{{$assessment->id}}' name="assessment[{{$user->id}}][{{$assessment->id}}]">
																{{$assessment->name}}
															</a>
	                        	@endforeach
													@else
														(empty)
													@endif
												</label>
                      </td>
                    </tr>
                  @endforeach
                @endif
              </tbody>
            </table>
          </div>
					{{csrf_field()}}
        </form>
      </div>
    </div>
  </div>
</section>
@include('pages.partials.result')
@endsection

@section('footerscript')
<script type="text/javascript">
  var assessment_url;

  $('a.result').click(function(e){
    assessment_url=$(this).attr('data-href');
    getResult(assessment_url);
    e.preventDefault();
  });

		$('.modal').on('hide.bs.modal', function () {
		  $('.modal-backdrop').remove();
		});

    function getResult(assessment_url){
			swal({
				title: "<i class='fa fa-spinner fa-pulse fa-2x fa-fw'></i>",
				html: "<h2>Please Wait...</h2>",
				showConfirmButton: false,
			});
			$.ajax({
				headers: {'X-CSRF-TOKEN': $('input[name="_token"]').val()},
				url:assessment_url,
				method: 'get',
				success:function(data){

					if(data.resultCode=='warning'){
						sweetAlert("Oops...", data.resultValue, "error");
						return false;
					}else if(data.resultCode=='success'){
						$('.result .user').html(data.resultValue.user);
						$('.result #question_set').html(data.resultValue.data);
					}
					swal.close();
					setTimeout(function(){
						$('.result').modal('show');
					},500);
				},error:function(data){
					sweetAlert("Oops...", "Error occured, Please try later..!", "error");
				}
			});
    }
    function updateAnswer(adata,user){
      $.ajax({
        headers: {'X-CSRF-TOKEN': $('input[name="_token"]').val()},
        url:'{{route('change_answer')}}',
        method: 'post',
        data:{'adata':adata,'user':user},
        success:function(data){
          if(data.resultCode=='warning'){
            sweetAlert("Oops...", "Error occured, Please try later..!", "error");
            return false;
          }else if(data.resultCode=='success'){
            $('.modal').modal('hide');
            getResult(assessment_url);
          }
          swal.close();
        },error:function(data){
          sweetAlert("Oops...", "Error occured, Please try later..!", "error");
        }
      });
    }

</script>
@endsection

