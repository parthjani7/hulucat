<footer class="main-footer">
  <div class="row">
    <div class="col-md-6 col-sm-6 col-xs-6">
      <span>&copy; Copyright  {{ date('Y') }} - <strong>Hulucat </strong></span>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-6">
      <span title='Parth Jani' class='pull-right'><strong><u>Handcrafted by <a href="http://parthjani.in">Parth Jani</a></u></strong></span>
    </div>
  </div>
</footer>
