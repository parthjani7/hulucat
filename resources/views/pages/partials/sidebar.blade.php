<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <li class="header">NAVIGATION PANEL</li>
      <li>
        <a href="/dashboard"><i class="fa fa-dashboard"></i>&nbsp;<span>Dashboard</span></a>
      </li>
      @hasanyrole('Administrator|Manager')
      <li class="treeview">
        <a href="#">
          <i class="ion ion-person-stalker"></i>
          <span>Staff</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="/staff"><i class="fa fa-circle-o"></i> Staff List</a></li>
          <li><a href="/staff/create"><i class="fa fa-circle-o"></i> Add Staff</a></li>
        </ul>
      </li>
      @endhasanyrole
      @hasanyrole('Administrator')
      <li class="treeview">
        <a href="#">
          <i class="fa fa-home"></i>
          <span>Shops</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="/shops"><i class="fa fa-circle-o"></i> Shop List</a></li>
          <li><a href="/shops/create"><i class="fa fa-circle-o"></i> Add Shops</a></li>
          <li><a href="/shops/assign"><i class="fa fa-circle-o"></i> Assign Shops</a></li>
        </ul>
      </li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-briefcase"></i>
          <span>Positions</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="/positions"><i class="fa fa-circle-o"></i> Position List</a></li>
          <li><a href="/positions/create"><i class="fa fa-circle-o"></i> Add Position</a></li>
          <li><a href="/positions/assign"><i class="fa fa-circle-o"></i> Assign Position</a></li>
        </ul>
      </li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-credit-card"></i>
          <span>&nbsp;Payment Methods</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{route('payment_methods.index')}}"><i class="fa fa-circle-o"></i> List</a></li>
          <li><a href="{{route('payment_methods.create')}}"><i class="fa fa-circle-o"></i> Create</a></li>
          <li><a href="{{route('payment_methods.showAssign')}}"><i class="fa fa-circle-o"></i> Assign</a></li>
          <!-- <li><a href="/positions/assign"><i class="fa fa-circle-o"></i> Assign Position</a></li> -->
        </ul>
      </li>
      @endhasanyrole
      @hasanyrole('Manager|Administrator')
      <li class="treeview">
        <a href="#">
          <i class="fa fa-calendar"></i>
          <span>Timesheet</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="/timesheets"><i class="fa fa-circle-o"></i> View Timesheet</a></li>
          <li><a href="/timesheets/create"><i class="fa fa-circle-o"></i>Create Timesheet</a></li>
        </ul>
      </li>
      @endhasanyrole
      @hasrole('Administrator')
      <li class="header">Advanced Settings</li>
      <!-- <li class="treeview">
        <a href="#">
          <i class="fa fa-bar-chart"></i>
          <span>User Assessment</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="/assessments"><i class="fa fa-circle-o"></i> List Assessments</a></li>
          <li><a href="/assessments/create"><i class="fa fa-circle-o"></i> Create Assessment</a></li>
          <li><a href="/assessments/assign"><i class="fa fa-circle-o"></i> Assign Assessment</a></li>
          <li><a href="/assessments/resultlist"><i class="fa fa-circle-o"></i> Results</a></li>
        </ul>
      </li> -->
      <li class="treeview">
        <a href="#">
          <i class="fa fa-shield"></i>
          <span>Roles & Permissions</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="/roles"><i class="fa fa-circle-o"></i> Roles</a></li>
          <!-- <li><a href="/roles/create"><i class="fa fa-circle-o"></i>Add Permission</a></li> -->
          <li><a href="/roles/grant"><i class="fa fa-circle-o"></i> Grant Roles</a></li>
        </ul>
      </li>
      @endhasrole

      @hasanyrole('Manager|Staff')
      <!-- <li class="treeview">
        <a href="#">
          <i class="fa fa-bar-chart"></i>
          <span>Assessments</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="/assessments/take"><i class="fa fa-circle-o"></i> Take Assessments</a></li>
          <li><a href="/assessments/taken"><i class="fa fa-circle-o"></i> Results</a></li>
        </ul>
      </li> -->
      @endhasanyrole
      @hasrole('Staff')
      <li class="treeview">
        <a href="#">
          <i class="fa fa-wpforms"></i>
          <span>EOD</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{route('sale.create')}}"><i class="fa fa-circle-o"></i> Add Sale</a></li>
        </ul>
      </li>
      @endhasrole
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>
