@extends('master')
@section('title','Position Assignments')
@section('pagename','Assign Position')
@section('contents')
<section class="content-header">
  <h1>@yield('pagename')</h1>
</section>
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      @include('pages.partials.success')
      <div class="box box-success">
        <form method="post">
          <input type="submit" style='margin:10px;' class='btn btn-success pull-right' value="Update Assignments">
          <div class="box-body">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th width='1'>#</th>
                  <th>Full Name</th>
                  <th>Email</th>
                  <th width='1'>Role</th>
                  <th>Assign Position</th>
                </tr>
              </thead>
              <tbody>
                @if($users->count()==0)
                  <tr><td colspan='5' align='center'><h4>Records not Found</h4></td></tr>
                @else
                  @foreach ($users as $user)
                    <tr>
                      <td>{{$loop->iteration}}</td>
                      <td>{{$user->firstname}} {{$user->lastname}}</td>
                      <td>{{$user->email}}</td>
                      <td>{{$user->Roles->first()->name}}</td>
                      <td>
                        @foreach ($user->positions as $position)
                          <label class="checkbox-inline" >
                            <input {{ ($user->positions->find($position->id)==null)?'':'checked'}} type="checkbox" name="position_ids[{{$user->id}}][]" value="{{$position->id}}">
                            {{$position->name}} (${{$position->hourly}})
                          </label>
                        @endforeach
                        @foreach ($positions->whereNotIn('id',$user->positions->pluck('id')) as $position)
                          <label class="checkbox-inline" >
                            <input type="checkbox" name="position_ids[{{$user->id}}][]" value="{{$position->id}}">
                            {{$position->name}} (${{$position->hourly}})
                          </label>
                        @endforeach
                      </td>
                    </tr>
                  @endforeach
                @endif
              </tbody>
            </table>
            {{csrf_field()}}
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
@endsection
@section('footerscript')
<script type="text/javascript">
$(document).ready(function(){
  $('.status_confirm').click(function(e){

  });

});
</script>
@endsection
