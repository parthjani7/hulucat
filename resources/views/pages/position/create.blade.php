@extends('master')
@section('title','Create Position')
@section('pagename','Add Position')
@section('headerscript')
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="/plugins/iCheck/all.css">
@endsection
@section('contents')
<section class="content-header">
  <h1>@yield('pagename')</h1>
</section>
<!-- Main content -->
<section class="content">

  @include('pages.partials.errors')
  @include('pages.partials.success')

  <form role="form" action='/positions' method='post'>
    <div class="row">
      <div class="col-md-12">
        <div class="box box-info">
          <div class="box-body">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Position Name</label>
                  <div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-briefcase"></i></div>
                    <input id="name" required name="name" placeholder='Enter Role Name' class="form-control" value="{{old('name')}}" type="text">
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Enter Hourly</label>
                  <div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-usd"></i></div>
                    <input id="hourly" required name="hourly" placeholder='Enter Hourly' class="form-control" value="{{old('hourly')}}" type="text">
                  </div>
                </div>
              </div>
            </div>
            <button type='submit' name="btnAdd" class="btn btn-success btn-md submit" id="btnAdd">Add Position</button>
          </div>
        </div>
      </div>
    </div>
    {{csrf_field()}}
  </form>
</section>
@endsection
@section('footerscript')
<script src="/plugins/iCheck/icheck.min.js"></script>
<script type="text/javascript">
$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
  checkboxClass: 'icheckbox_minimal-blue',
  radioClass: 'iradio_minimal-blue'
});
</script>
@endsection
