@extends('master')
@section('title','Edit Position')
@section('pagename','Position Information')
@section('contents')
<section class="content-header">
  <h1>@yield('pagename')</h1>
</section>
<!-- Main content -->
<section class="content">
    @include('pages.partials.errors')
  <form role="form" action='/positions/{{$position->id}}/update' method='post'>
    <div class="row">
      <div class="col-md-12">
        <div class="box box-success">
          <div class="box-body">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Position Name</label>
                  <div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-briefcase"></i></div>
                    <input id="name" required name="name" value="{{$position->name}}" placeholder='Enter Role Name' class="form-control" value="{{old('name')}}" type="text">
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Enter Hourly</label>
                  <div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-usd"></i></div>
                    <input id="hourly" value="{{$position->hourly}}" required name="hourly" placeholder='Enter Hourly' class="form-control" value="{{old('name')}}" type="text">
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label>Status : </label>&nbsp;
              <label>
                <input type="radio" {{($position->status=='1')?'checked':''}} required name="status" value='1' class="minimal"> Active
              </label>
              <label>
                <input type="radio" {{($position->status=='0')?'checked':''}} required name="status" class="minimal" value='0'> Inactive
              </label>
            </div>
            <button type='submit' name="btnUpdate" class="btn btn-success btn-md submit" id="btnUpdate">Update Role</button>
          </div>
        </div>
      </div>
    </div>
    {{ csrf_field() }}
    {{ method_field('PATCH') }}
  </form>
</section>
@endsection
