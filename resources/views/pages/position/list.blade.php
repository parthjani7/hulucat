@extends('master')
@section('title','Positions')
@section('pagename','Positions')
@section('contents')
<section class="content-header">
  <h1>@yield('pagename')</h1>
</section>
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      @include('pages.partials.success')
      <div class="box box-success">
        <div class="box-header">
          <a href='/positions/create' class='btn btn-success'><i class="fa fa-user-plus"></i> Add More</a>
          <!-- <h3 class="box-title">Hover Data Table</h3> -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <!-- @if(isset($status))
            <div class="alert alert-success">
              {{$status}}
            </div>
          @endif -->
          <table id="rolelist" class="table table-bordered table-hover">
            <thead>
              <tr>
                <th>#</th>
                <th>Position</th>
                <th>Hourly</th>
                <th>Status</th>
                <th>Creation Date</th>
                <th class='text-center'>Action</th>
              </tr>
            </thead>
            <tbody>
              @if($positions->count()==0)
                <tr><td colspan='5' align='center'><h4>Records not Found</h4></td></tr>
              @else
                @foreach ($positions as $position)
                  <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$position->name}}</td>
                    <td>{{$position->hourly}}</td>
                    <td>
                      <a href='{{route('position_toggle',$position->id)}}' class='toggleStatus' title='click to toggle'>
                        <span class="label bg-{{($position->status=='1')?'green':'red'}}">
                          {{($position->status=='1')?'Active':'Inactive'}}
                        </span>
                      </a>
                    </td>
                    <td>{{(isset($position->created_at))?$position->created_at->toDateString():''}}</td>
                    <td align='center'><a title='Edit' class='purple' href="positions/edit/{{$position->id}}"><i class='fa fa-pencil'></i></a></td>
                  </tr>
                @endforeach
              @endif
            </tbody>
          </table>
          {{csrf_field()}}
        </div>
        <!-- /.box-body -->
      </div>
    </div>
  </div>
</section>
@endsection
@section('footerscript')
<script type="text/javascript">
$(document).ready(function(){
  function toggleLabel(label,val){
    console.info(val);
    if(val==false){
      label.removeClass('bg-green');
      label.addClass('bg-red');
      label.html('Inactive')
    }else{
      label.removeClass('bg-red');
      label.addClass('bg-green');
      label.html('Active')
    }
  }

});
</script>
@endsection
