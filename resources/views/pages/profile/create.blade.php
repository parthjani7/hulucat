@extends('master')
@section('title','Create Staff')
@section('pagename','Add Staff')
@section('contents')
<section class="content-header">
  <h1>@yield('pagename')</h1>
</section>
<!-- Main content -->
<section class="content">
  @if (count($errors))
    <div class="alert alert-danger alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <h4><i class="icon fa fa-ban"></i> Alert!</h4>
      <ul class='list-unstyled'>
        {!! implode('', $errors->all('<li>:message</li>')) !!}
      </ul>
    </div>
  @endif

  <form role="form" action='/staff' method='post'>
    <div class="row">
      <div class="col-md-6">
        <div class="box box-success">
          <div class="box-header with-border">
            <h3 class="box-title">Login</h3>
          </div>
          <div class="box-body">
            <div class="form-group">
              <label>Username</label>
              <input required id="username" name="username" placeholder='Enter Username' class="form-control" value="{{old('username')}}" type="text">
            </div>
            <div class="form-group">
              <label>Email Address</label>
              <input required id="email" name="email" placeholder='Enter Email' class="form-control" value="{{old('email')}}" type="email">
            </div>
            <div class="form-group">
              <label>Enter Password</label>
              <input type='password' name="password" class="form-control" placeholder='Password' value="{{old('password')}}" required id="password" requird>
            </div>
          </div>
        </div>
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Personal Information</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <div class="box-body">
            <div class="form-group">
              <label>First Name</label>
              <input name="firstname" class="text form-control small" value="{{old('firstname')}}" required id="firstname" placeholder='Enter Firstname' type="text">
            </div>
            <div class="form-group">
              <label>Last Name</label>
              <input name="lastname" placeholder='Enter Lastname' class="text form-control small" value="{{old('lastname')}}" required id="lastname" type="text">
            </div>
            <div class="form-group">
              <label>Date of Birth</label>
              <div class="input-group date">
                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                <input value="{{old('birthdate')}}" id="birthdate" name="birthdate" placeholder='Select DOB' readonly class="form-control pull-right datepicker" type="text">
              </div>
            </div>
            <div class="form-group">
              <label>Blood Type</label>
              <select name="blood_type" class="form-control text" id="blood_type">
                <option value="">Select Option</option>
                <option value="A">A</option>
                <option value="B">B</option>
                <option value="AB">AB</option>
                <option value="O">O</option>
              </select>
            </div>
            <div class="form-group">
              <label>Country of Origin</label>
              <input name="origin_country" placeholder='Enter Country' class="text form-control small" value="{{old('origin_country')}}" id="origin_country" type="text">
            </div>
            <div class="form-group">
              <label>Passport Number</label>
              <input name="passport_num" placeholder='Enter Passport Number'  class="text form-control small" value="{{old('passport_num')}}" id="passport_num" type="text">
            </div>
            <div class="form-group">
              <label>Visa Type</label>
              <select name="visa_type" class="form-control text" value="{{old('visa_type')}}" id="visa_type">
                <option value="">Select Option</option>
                <option value="WV">Working Visa</option>
                <option value="SV">Student Visa</option>
                <option value="PR">PR/Citizen</option>
              </select>
            </div>
            <div class="form-group">
              <label>Work Permit</label>
              <select name="work_permit" class="form-control text" value="{{old('work_permit')}}" id="work_permit">
                <option value="">Select Option</option>
                <option value="1">Yes</option>
                <option value="0">No</option>
              </select>
            </div>
            <div class="form-group">
              <label>IRD Number</label>
              <input name="ird_number" placeholder='Enter IRD'  class="text form-control small" value="{{old('ird_number')}}" id="ird_number" type="text">
            </div>
            <div class="form-group">
              <label>Tax Code</label>
              <input name="tax_code" class="text form-control small" placeholder='Enter Tax Code' value="{{old('tax_code')}}" id="tax_code" type="text">
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title">Employment</h3>
          </div>
          <div class="box-body">
            <div class="form-group">
              <label>Employement Date</label>
              <div class="input-group date">
                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                <input value="{{old('emp_date')}}" id="emp_date" name="emp_date" placeholder='Select Date' readonly class="form-control pull-right datepicker" type="text">
              </div>
            </div>
            <div class="form-group">
              <label>Dedicated Shop</label>
              <div class="radio">
              <label><input value="23" name="dedicated_shop" type="radio">BCL</label>
              </div>
            </div>
            <div class="form-group">
              <label>Additional Shop</label>
              <div class="checkbox">
                <label>
                  <input value="9" name="shop_ids[]" type="checkbox">Discretelogix
                </label>
              </div>
            </div>
            <div class="form-group">
              <label>User Type</label>
              <div class="radio">
                <label><input value="0" name="role_id" type="radio">Staff</label>
                &nbsp;&nbsp;&nbsp;
                <label><input value="2" name="role_id" type="radio">Manager</label>
              </div>
            </div>
            <div class="form-group">
              <label>Rate 1</label>
              <input class="form-control text" name="rate1" type="text">
            </div>
            <div class="form-group">
              <label>Rate 2(Salary)</label>
              <input class="form-control text" name="rate2" type="text">
            </div>
            <div class="role_rate_wrapper">
              <div class="roles">
                <div class="form-group pull-left">
                  <label>Role Name</label>
                  <select class="form-control rolelist" name="role_name[]">
                    <option value="">-- Select Role --</option>
                    <option data-val="12.50" value="7">Waiter</option>
                    <option data-val="10.50" value="6">Regular Staff</option>
                    <option data-val="11.25" value="5">Administrator</option>
                  </select>
                </div>
                <div class="form-group pull-left">
                  <label>Role Rate</label>
                  <input class="form-control text" readonly type="text">
                </div>
              </div>
            </div>
            <span class="role_rate_placeholder"></span>
            <div class="form-group">
              <button type="button" class="btn btn-primary btn-block">Add Role</button>
            </div>
            <div class="form-group">
              <label>Eligible Working Hours</label>
              <input class="form-control text" value="{{old('max_hours')}}" name="max_hours" value="1" min="1" type="number">
            </div>
            <div class="form-group">
              <label>MBTI Type</label>
              <select name="mbit_type" class="form-control text" id="mbit_type">
                <option value="">Select Option</option>
                <option>ISTJ</option><option>ISFJ</option>
                <option>INFJ</option><option>ENFJ</option><option>ENTJ</option>
                <option>INTJ</option><option>ISFJ</option><option>ISTP</option>
                <option>ISFP</option><option>INFP</option><option>INTP</option>
                <option>ESTP</option><option>ESFP</option><option>ENFP</option>
                <option>ENTP</option><option>ESTJ</option><option>ESFJ</option>
              </select>
            </div>
            <div class="form-group">
              <label>Status</label>
              <div class="radio">
                <label><input value="1" checked name="status" type="radio">Active</label> &nbsp;&nbsp;&nbsp;
                <label><input value="0" name="status" type="radio">Inactive</label>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    {{csrf_field()}}
    <button type='submit' name="btnAdd" class="btn btn-success btn-md submit" id="btnAdd">Add Staff</button>
  </form>
</section>
@endsection
@section('footerscript')
<script>
$('.datepicker').datepicker({autoclose: true,format:'yyyy-mm-dd'});
</script>
@endsection
