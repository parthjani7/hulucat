@extends('master')
@section('title','Edit Profile')
@section('pagename','Profile Information')
@section('contents')
<section class="content-header">
  <h1>@yield('pagename')</h1>
</section>
<!-- Main content -->
<section class="content">
  @include('pages.partials.errors')
  @include('pages.partials.success')
  <form role="form" action='/profile/update' method='post'>
    <div class="row">
      <div class="col-md-6">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Login</h3>
          </div>
          <div class="box-body">
            <div class="form-group">
              <label>Username</label>
              <input required id="username" name="username" placeholder='Enter Username' class="form-control" value='{{$user->username}}' type="text">
            </div>
            <div class="form-group">
              <label>Email Address</label>
              <input required id="email" name="email" placeholder='Enter Email'  class="form-control" value='{{$user->email}}' type="email">
            </div>
            <div class="form-group">
              <label>Enter Password</label>
              <input type='password' name="password" class="form-control" placeholder='Password' id="password" >
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Personal Information</h3>
          </div>
          <div class="box-body">
            <div class="form-group">
              <label>First Name</label>
              <input required name="firstname" class="text form-control small"  id="firstname" value='{{$user->firstname}}' placeholder='Enter Firstname' type="text">
            </div>
            <div class="form-group">
              <label>Last Name</label>
              <input required name="lastname" placeholder='Enter Lastname'  class="text form-control small" value='{{$user->lastname}}' id="lastname" type="text">
            </div>
            <div class="form-group">
              <label>Date of Birth</label>
              <div class="input-group date">
                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                <input id="birthdate" name="birthdate" placeholder='Select DOB' readonly value='{{$user->birthdate}}' class="form-control pull-right datepicker" type="text">
              </div>
            </div>
            <div class="form-group">
              <label>Blood Type</label>
              <select name="blood_type" class="form-control text" id="blood_type">
                <option value="">Select Option</option>
                <option {{($user->blood_type=='A')?'selected':''}} value="A">A</option>
                <option {{($user->blood_type=='B')?'selected':''}} value="B">B</option>
                <option {{($user->blood_type=='AB')?'selected':''}} value="AB">AB</option>
                <option {{($user->blood_type=='O')?'selected':''}} value="O">O</option>
              </select>
            </div>

            <div class="form-group">
              <label>Country of Origin</label>
              <input name="origin_country" placeholder='Enter Country' class="text form-control small" value='{{$user->origin_country}}' id="origin_country" type="text">
            </div>
            <div class="form-group">
              <button type='submit' name="btnAdd" class="btn btn-primary btn-md" id="btnUpdate">Update Profile</button>
            </div>
          </div>

        </div>
      </div>
    </div>
    {{ csrf_field() }}
    {{method_field('PATCH')}}
  </form>
</section>
@endsection
@section('footerscript')
<script>
$('.datepicker').datepicker({autoclose: true,format:'yyyy-mm-dd'});
</script>
@endsection
