@extends('master')
@section('title','Staff List')
@section('pagename','Staff')
@section('contents')
<section class="content-header">
  <h1>@yield('pagename')</h1>
</section>
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      @include('pages.partials.success')
      <div class="box box-success">
        <div class="box-header">
          <a href='/staff/create' class='btn btn-success'><i class="fa fa-user-plus"></i> Add More</a>
          <!-- <h3 class="box-title">Hover Data Table</h3> -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <!-- @if(isset($status))
            <div class="alert alert-success">
              {{$status}}
            </div>
          @endif -->
          <table id="stafflist" class="table table-bordered table-hover">
            <thead>
              <tr>
                <th>Full Name</th>
                <th>Email</th>
                <th>Status</th>
                <!-- <th>Permission</th> -->
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              @if($users->count()==0)
                <tr><td colspan='5' align='center'><h4>Records not Found</h4></td></tr>
              @else
                @foreach ($users as $user)
                  <tr>
                    <td>{{$user->firstname}} {{$user->lastname}}</td>
                    <td>{{$user->email}}</td>
                    <td>
                      <small class="label bg-{{($user->status=='1')?'green':'red'}}">
                        {{($user->status=='1')?'Active':'Inactive'}}
                      </small>
                    </td>
                    <!-- <td></td> -->
                    <td><a title='Edit' href="staff/edit/{{$user->id}}"><i class='fa fa-pencil'></i></a> &nbsp; <a title='Toggle status' class='status_confirm' href="staff/status/{{$user->id}}"><i class='fa fa-user-o'></i></a></td>
                  </tr>
                @endforeach
              @endif
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
  </div>
</section>
@endsection
@section('script')
<script type="text/javascript">
$(document).ready(function(){
  $('.status_confirm').click(function(e){
    if(!confirm('Are you sure you want to change the user\'s status ?')){
      e.preventDefault();
      return false;
    }
  });
});
</script>
@endsection
