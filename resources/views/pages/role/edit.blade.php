@extends('master')
@section('title','Edit Role')
@section('pagename','Role Information')
@section('contents')
<section class="content-header">
  <h1>@yield('pagename')</h1>
</section>
<!-- Main content -->
<section class="content">
    @include('pages.partials.errors')
  <form role="form" action='/roles/{{$role->id}}/update' method='post'>
    <div class="row">
      <div class="col-md-12">
        <div class="box box-success">
          <div class="box-body">
            <div class="form-group">
              <label>Role Name</label>
              <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-shield"></i></div>
                <input id="name" required name="name" placeholder='Enter Role Name' class="form-control" value="{{$role->name}}" type="text">
              </div>
            </div>
            <div class="form-group">
              <label>Role Status : </label>&nbsp;
              <label>
                <input type="radio" {{($role->status=='1')?'checked':''}} required name="status" value='1' class="minimal"> Active
              </label>
              <label>
                <input type="radio" {{($role->status=='0')?'checked':''}} required name="status" class="minimal" value='0'> Inactive
              </label>
            </div>
            <button type='submit' name="btnUpdate" class="btn btn-success btn-md submit" id="btnUpdate">Update Role</button>
          </div>
        </div>
      </div>
    </div>
    {{ csrf_field() }}
    {{ method_field('PATCH') }}
  </form>
</section>
@endsection
