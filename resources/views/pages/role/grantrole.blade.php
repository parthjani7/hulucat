@extends('master')
@section('title','Grant Roles')
@section('pagename','Grant Roles')
@section('contents')
<section class="content-header">
  <h1>@yield('pagename')</h1>
</section>
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      @include('pages.partials.success')
      <div class="box box-success">
        <div class="box-header">
          <!-- <a href='/roles/create' class='btn btn-success'><i class="fa fa-user-plus"></i> Add Role</a> -->
          <!-- <h3 class="box-title">Hover Data Table</h3> -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <!-- @if(isset($status))
            <div class="alert alert-success">
              {{$status}}
            </div>
          @endif -->
          <table id="grantrolelist" class="table table-bordered table-hover">
            <thead>
              <tr>
                <th>#</th>
                <th>User</th>
                <th>Dedicated Shop</th>
                <th>Role Name</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              @if($users->count()==0)
                <tr><td colspan='5' align='center'><h4>Records not Found</h4></td></tr>
              @else
                @foreach ($users as $user)
                  <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$user->fullname}}</td>
                    <td>
                        @if(isset($user->dedicated))
                          {{$user->filteredShops(true)->name}}
                        @else
                          <small class='label bg-red'>Unassigned</small>
                        @endif
                    </td>
                    <td>
                      <select name="role_id" class="form-control text" id="role_id{{$user->id}}">
                        <option value="">Select Option</option>
                        @foreach ($roles as $role)
                          <option {{($user->roles()->first()->id==$role->id)?'selected':''}} value="{{$role->id}}">{{$role->name}}</option>
                        @endforeach
                        </select>
                    </td>
                    <td><button data-user-id='{{$user->id}}' type='button' class='btn btn-primary btn-update'>Update</button></td>
                  </tr>
                @endforeach
              @endif
            </tbody>
          </table>
          {{csrf_field()}}
        </div>
        <!-- /.box-body -->
      </div>
    </div>
  </div>
</section>
@endsection
@section('footerscript')
<script type="text/javascript">
$(document).ready(function(){
  $('.status_confirm').click(function(e){

  });
  $('.btn-update').click(function(e){
    var user_id=$(this).attr('data-user-id');
    var role_id=$('#role_id'+user_id).val();
    $.ajax({
      headers: {
        'X-CSRF-TOKEN': $('input[name="_token"]').val()
      },
      url:'/roles/grant/'+user_id,
      data:{'role_id':role_id},
      method:'post',
      success:function(res){
        if(res.resultCode=='Success'){
          alert(res.message);
        }
      }
    });
    e.preventDefault();
  })

});
</script>
@endsection
