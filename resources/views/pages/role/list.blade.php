@extends('master')
@section('title','Roles')
@section('pagename','Roles')
@section('contents')
<section class="content-header">
  <h1>@yield('pagename')</h1>
</section>
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      @include('pages.partials.success')
      <div class="box box-success">
        <div class="box-header">
          <a href='/roles/create' class='btn btn-success'><i class="fa fa-user-plus"></i> Add More</a>
          <!-- <h3 class="box-title">Hover Data Table</h3> -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <!-- @if(isset($status))
            <div class="alert alert-success">
              {{$status}}
            </div>
          @endif -->
          <table id="rolelist" class="table table-bordered table-hover">
            <thead>
              <tr>
                <th>#</th>
                <th>Role Name</th>
                <th>Status</th>
                <th>Creation Date</th>
                <th class='text-center'>Action</th>
              </tr>
            </thead>
            <tbody>
              @if($roles->count()==0)
                <tr><td colspan='5' align='center'><h4>Records not Found</h4></td></tr>
              @else
                @foreach ($roles as $role)
                  <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$role->name}}</td>
                    <td>
                      <a href='{{route('role_toggle',$role->id)}}' class='toggleStatus' title='click to toggle'>
                        <span class="label bg-{{($role->status=='1')?'green':'red'}}">
                          {{($role->status=='1')?'Active':'Inactive'}}
                        </span>
                      </a>
                    </td>
                    <td>{{(isset($role->created_at))?$role->created_at->toDateString():''}}</td>
                    <td align='center'><a title='Edit' class='purple' href="roles/edit/{{$role->id}}"><i class='fa fa-pencil'></i></a></td>
                  </tr>
                @endforeach
              @endif
            </tbody>
          </table>
          {{csrf_field()}}
        </div>
        <!-- /.box-body -->
      </div>
    </div>
  </div>
</section>
@endsection
@section('footerscript')
<script type="text/javascript">
$(document).ready(function(){
  $('.status_confirm').click(function(e){

  });
  $('a.ajax').click(function(e){
    if(!confirm('Are you sure you want to change the user\'s status ?')){
      return false;
    }
    var label=$(this);
    $.ajax({
      headers: {
        'X-CSRF-TOKEN': $('input[name="_token"]').val()
      },
      url:label.attr('href'),
      method:'post',
      success:function(res){
        if(res.resultCode=='success'){
          toggleLabel(label.find('span.label'),res.status);
        }
      }
    });
    e.preventDefault();
  })
  function toggleLabel(label,val){
    console.info(val);
    if(val==false){
      label.removeClass('bg-green');
      label.addClass('bg-red');
      label.html('Inactive')
    }else{
      label.removeClass('bg-red');
      label.addClass('bg-green');
      label.html('Active')
    }
  }

});
</script>
@endsection
