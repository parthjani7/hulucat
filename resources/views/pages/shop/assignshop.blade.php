@extends('master')
@section('title','Shop Assignments')
@section('pagename','Assign Shop')
@section('contents')
<section class="content-header">
  <h1>@yield('pagename')</h1>
</section>
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      @include('pages.partials.success')
      <div class="box box-success">
        <form method="post">
          <input type="submit" style='margin:10px;' class='btn btn-success pull-right' value="Update Assignments">
          <div class="box-body">
            <table id="stafflist" class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th width='1'>#</th>
                  <th>Full Name</th>
                  <th>Email</th>
                  <th width='1'>Role</th>
                  <th>Status</th>
                  <th>Dedicated Shop</th>
                  <th>Additional Shop</th>
                </tr>
              </thead>
              <tbody>
                @if($users->count()==0)
                  <tr><td colspan='5' align='center'><h4>Records not Found</h4></td></tr>
                @else
                  @foreach ($users as $user)
                    <tr>
                      <td>{{$loop->iteration}}</td>
                      <td>{{$user->firstname}} {{$user->lastname}}</td>
                      <td>{{$user->email}}</td>
                      <td>
                        <a href='/staff/toggle/{{$user->id}}' class='ajax' title='click to toggle'>
                          <span class="label bg-{{($user->status=='1')?'green':'red'}}">
                            {{($user->status=='1')?'Active':'Inactive'}}
                          </span>
                        </a>
                      </td>
                      <td>{{$user->Roles->first()->name}}</td>
                      <td><select class="form-control" name="dedicated_shop[{{$user->id}}][]">
                        <option value=''> -- SHOP LIST-- </option>
                        @foreach ($shoplist as $shop)
                          <option {{($shop->id==$user->dedicated)?'selected':''}} value='{{$shop->id}}'>{{$shop->name}}</option>
                        @endforeach
                      </select></td>
                      <td>
                        @foreach ($user->shops as $shop)
                          <label class="checkbox-inline {{ ($shop->id==$user->dedicated)?'hidden':''}}" >
                            <input {{ (in_array($shop->id,$user->additional))?'checked':''}} type="checkbox" class='additional' name="shop_ids[{{$user->id}}][]" id="additional{{$shop->id}}" value="{{$shop->id}}">{{$shop->name}}
                          </label>
                        @endforeach
                        @foreach ($shoplist->whereNotIn('id',$user->shops->pluck('id')->toArray()) as $shop)
                          <label class="checkbox-inline {{ ($shop->id==$user->dedicated)?'hidden':''}}" >
                            <input {{ (in_array($shop->id,$user->additional))?'checked':''}} type="checkbox" class='additional' name="shop_ids[{{$user->id}}][]" id="additional{{$shop->id}}" value="{{$shop->id}}">{{$shop->name}}
                          </label>
                        @endforeach
                      </td>
                    </tr>
                  @endforeach
                @endif
              </tbody>
            </table>
            {{csrf_field()}}
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
@endsection
@section('footerscript')
<script type="text/javascript">
$(document).ready(function(){
  $('a.ajax').click(function(e){
    if(!confirm('Are you sure you want to change the user\'s status ?')){
      return false;
    }
    var label=$(this);
    $.ajax({
      headers: {
        'X-CSRF-TOKEN': $('input[name="_token"]').val()
      },
      url:label.attr('href'),
      method:'post',
      success:function(res){
        if(res.resultCode=='success'){
          toggleLabel(label.find('span.label'),res.status);
        }
      }
    });
    e.preventDefault();
  })
  function toggleLabel(label,val){
    if(val==false){
      label.removeClass('bg-green');
      label.addClass('bg-red');
      label.html('Inactive')
    }else{
      label.removeClass('bg-red');
      label.addClass('bg-green');
      label.html('Active')
    }
  }

});
</script>
@endsection
