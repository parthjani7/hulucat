@extends('master')
@section('title','Create Shop')
@section('pagename','Add Shop')
@section('headerscript')
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="/plugins/iCheck/all.css">
@endsection
@section('contents')
<section class="content-header">
  <h1>@yield('pagename')</h1>
</section>
<!-- Main content -->
<section class="content">

  @include('pages.partials.errors')
  @include('pages.partials.success')

  <form role="form" action='/shops' method='post'>
    <div class="row">
      <div class="col-md-12">
        <div class="box box-info">
          <div class="box-body">
            <div class="form-group">
              <label>Shop Name</label>
              <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-university"></i></div>
                <input id="name" required name="name" placeholder='Enter Shop Name' class="form-control" value="{{old('name')}}" type="text">
              </div>
            </div>
            <div class="form-group">
              <label>Shop Status : </label>&nbsp;
              <label>
                <input type="radio" required name="status" value='1' class="minimal" checked>
                Active
              </label>
              <label>
                <input type="radio" required name="status" class="minimal" value='0'>
                Inactive
              </label>
            </div>
            <button type='submit' name="btnAdd" class="btn btn-success btn-md submit" id="btnAdd">Add Shop</button>
          </div>
        </div>
      </div>
    </div>
    {{csrf_field()}}
  </form>
</section>
@endsection
@section('footerscript')
<script src="/plugins/iCheck/icheck.min.js"></script>
<script type="text/javascript">
$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
  checkboxClass: 'icheckbox_minimal-blue',
  radioClass: 'iradio_minimal-blue'
});
</script>
@endsection
