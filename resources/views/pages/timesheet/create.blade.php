@extends('master')
@section('title','Create Timesheet')
@section('pagename','Create Timesheet')

@section('contents')
<section class="content-header">
  <style media="screen">
    /*.calendar-table{display: none;}*/
  </style>
  <h1>@yield('pagename')</h1>
</section>
<!-- Main content -->
<section class="content">

  @include('pages.partials.errors')
  @include('pages.partials.success')

  <form role="form" action='find/staff/' id='timesheet_form'>
    <div class="row">
      <div class="col-md-12">
        <div class="box box-info">
          <div class="box-body">
            <div class="row">
              <div class="col-md-5 col-sm-4">
                <div class="form-group">
                  <label>Select Shop</label>
                  <select class="form-control" required id="shop" name="shop">
                    <option value=''> -- SELECT SHOP -- </option>
                    @foreach ($shoplist as $key=>$shop)
                      <option value='{{$key}}'>{{$shop}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="col-md-5 col-sm-4">
                <div class="form-group">
                  <label>Select Date</label>
                  <div class="input-group date">
                    <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                    <input required value="{{old('date')}}" id="date" name="date" placeholder='Select Date' readonly class="form-control pull-right datepicker" value="{{Date('Y-m-d')}}" type="text">
                  </div>
                </div>
              </div>
              <div class="col-md-2 col-sm-4">
                <label></label>
                <button type='submit' class="btn btn-success form-control" id="btnAddTimesheet">Create</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    {{csrf_field()}}
  </form>
</section>
@include('pages.timesheet.modalform')
@endsection

@section('footerscript')
<script src="/plugins/iCheck/icheck.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    var today = "{{$dates['today']}}";
    $('.datepicker').datepicker({
      todayHighlight: true,
      autoclose: true,
      @hasrole('Manager')
        startDate:today,
        endDate:today,
      @endhasrole
      format:'yyyy-mm-dd'
    }).datepicker('setDate',today);

    $(".timepicker").timepicker({
      showInputs: false
    });

    $('#timesheet_form').submit(function(e){
      var date=$('#date').val();
      var shop=$('#shop').val()

      $.ajax({
        headers: {
          'X-CSRF-TOKEN': $('input[name="_token"]').val()
        },
        url:$(this).attr('action')+shop,
        method:'post',
        success:function(res){
          if(res.resultCode=='Success'){
            $('.timesheet_modal #timesheet_date').val(date);
            $('.timesheet_modal #shop_id').val(shop);
            var staff='<option value="">-- Selecect Staff --</option>';
            $.each(res.resultValue,function(key,value){
              staff+='<option value="'+value.id+'">'+value.fullname+'</option>'
            });
            $('.timesheet_modal #select_staff').html(staff);
            $('.timesheet_modal').modal('show');

          }else if(res.resultCode=='Warning'){
            alert(res.resultValue);
          }
        }
      });
      e.preventDefault();
    });
    $('#select_staff').change(function(){
      if(!$(this).val())
        return false;
      var id=$(this).val();

      $.ajax({
        headers: {
          'X-CSRF-TOKEN': $('input[name="_token"]').val()
        },
        type:"post",
        url:"/timesheets/find/positions/"+$(this).val(),
        success: function(res){
          console.info(res);
          if(res.resultCode=='Success'){
            var option="";
            $.each(res.resultValue,function(key,val){
              option+='<option value=\''+key+'\'>'+val+'</option>';
            });
            $('#select_role').html(option);
          }else{
            alert(res.resultValue);
          }
        }
      });
    });
});

</script>
@endsection
