@extends('master')
@section('title','Edit Timesheet')
@section('pagename','Edit Timesheet')

@section('contents')
<section class="content-header">
  <h1>@yield('pagename')</h1>
</section>
<!-- Main content -->
<section class="content">

  @include('pages.partials.errors')
  @include('pages.partials.success')

  <div class="row">
    <div class="col-md-12">
      <div class="box box-info">
        <div class="box-body">
          <form role="form" action='/timesheets/find/staff/' id='timesheet_form'>
            <div class="row">
              <div class="col-md-5 col-sm-4">
                <div class="form-group">
                  <label>Select Shop</label>
                  <select class="form-control" required id="shop" name="shop">
                    <option value='{{$shoplist->id}}'>{{$shoplist->name}}</option>
                  </select>
                </div>
              </div>
              <div class="col-md-5 col-sm-4">
                <div class="form-group">
                  <label>Select Date</label>
                  <div class="input-group date">
                    <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                    <input required id="date" name="date" placeholder='Select Date' readonly class="form-control pull-right" value="{{$timesheet->date}}" type="text">
                  </div>
                </div>
              </div>
              <div class="col-md-2 col-sm-4">
                <label></label>
                <button type='submit' class="btn btn-success form-control" id="btnAddTimesheet">Create</button>
              </div>
            </div>
            {{csrf_field()}}
          </form>
        </div>
        <div class="row">
          <div class="col-md-12">
            <table id="timesheetslist" class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th>Staff</th>
                  <th>Position</th>
                  <th>Shop Name</th>
                  <th>Start Time</th>
                  <th>End Time</th>
                  <th>Break</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                @if($timesheet->count()==0)
                  <tr><td colspan='5' align='center'><h4>Records not Found</h4></td></tr>
                @else
                  @foreach ($timesheet->records as $record)
                    <tr>
                      <td>{{$record->user->fullname}}</td>
                      <td>
                        {{$record->role_name}} -
                        ({{$record->role_rate}})
                      </td>
                      <td>{{$record->shop_name}}</td>
                      <!-- <td>{{explode(' ',$record->start_time)[1]}}</td> -->
                      <td>{{Date('H:i A',strtotime($record->start_time))}}</td>
                      <td>{{Date('H:i A',strtotime($record->end_time))}}</td>
                      <!-- <td>{{explode(' ',$record->end_time)[1]}}</td> -->
                      <td>{{($record->break==0)?'--':$record->break}}</td>
                      <td><a class='action edit' title='Edit' href="{{$record->id}}"><i class='fa fa-pencil purple'></i></a> &nbsp;
                      <a class='action delete' title='Delete' href="{{$record->id}}"><i class='fa purple fa-trash'></i></a> &nbsp;
                      <span class='action' type="button" data-toggle="tooltip" data-placement="bottom" title='Remark : {{($record->remark!='')?$record->remark:'(Empty)'}}'><i class='fa purple fa-comment-o'></i></span></td>
                    </tr>
                  @endforeach
                @endif
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@include('pages.timesheet.modalform')
@endsection
@section('footerscript')
<script src="/plugins/iCheck/icheck.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
  $('#datetimerange').daterangepicker({
    timePicker: true,
    timePickerIncrement:5,
    locale: {
      format: 'h:mm A',
      "startDate": '{{$timesheet->date}}',
      "endDate": "{{$timesheet['endDate']}}"
    }
  });
  $('a.edit').click(function(e){
    var td=$(this);
    var id=td.attr('href');

    $.ajax({
      headers: {
        'X-CSRF-TOKEN': $('input[name="_token"]').val()
      },
      url:"/timesheets/record/"+id,
      method:'post',
      success:function(res){
        if(res.resultCode=='Success'){
          option=staff='';
          //$('.timesheet_modal #timesheet_date').val(date);
          //$('.timesheet_modal #shop_id').val(shop);
          //var staff='<option value="">-- Selecect Staff --</option>';
          $.each(res.resultValue,function(key,value){
            if(key=='user'){
              $.each(value,function(key1,value1){
                staff+='<option value="'+key1+'">'+value1+'</option>'
              });
              $('.timesheet_modal #select_staff').html(staff);
            }
            if(key=='positions'){
              $.each(value,function(key1,value1){
                option+='<option value=\''+key1+'\'>'+value1+'</option>';
              });
              $('#select_role').html(option);
            }

          });

          $('.timesheet_modal').modal('show');
        }else if(res.resultCode=='Warning'){
          alert(res.resultValue);
        }
      }
    });
    e.preventDefault();
  });


  $('#timesheet_form').submit(function(e){
    var date=$('#date').val();
    var shop=$('#shop').val()

    $.ajax({
      headers: {
        'X-CSRF-TOKEN': $('input[name="_token"]').val()
      },
      url:$(this).attr('action')+shop,
      method:'post',
      success:function(res){
        if(res.resultCode=='Success'){
          $('.timesheet_modal #timesheet_date').val(date);
          $('.timesheet_modal #shop_id').val(shop);
          var staff='<option value="">-- Selecect Staff --</option>';
          $.each(res.resultValue,function(key,value){
            staff+='<option value="'+key+'">'+value+'</option>'
          });
          $('.timesheet_modal #select_staff').html(staff);
          $('.timesheet_modal').modal('show');
        }else if(res.resultCode=='Warning'){
          alert(res.resultValue);
        }
      }
    });
    e.preventDefault();
  });


  $('#select_staff').change(function(){
    if(!$(this).val()){return false;}
    var id=$(this).val();

    $.ajax({
      headers: {
        'X-CSRF-TOKEN': $('input[name="_token"]').val()
      },
      type:"post",
      url:"/timesheets/find/positions/"+$(this).val(),
      success: function(res){
        if(res.resultCode=='Success'){
          var option="";
          $.each(res.resultValue,function(key,val){
            option+='<option value=\''+key+'\'>'+val+'</option>';
          });
          $('#select_role').html(option);
        }else{
          alert(res.resultValue);
        }
      }
    });
  });


  $('a.delete').click(function(e){
    if(!confirm('Are you sure you want to delete this record ?'))
      return false;
      var td=$(this);
      var id=td.attr('href');
      $.ajax({
        headers: {
          'X-CSRF-TOKEN': $('input[name="_token"]').val()
        },
        type:"post",
        url:"/timesheets/delete/record/"+id,
        success: function(res){
          if(res.resultCode=='Success'){
            console.info(td.closest('tr'))
            td.closest('tr').remove();
          }else{
            alert(res.resultValue);
          }
        }
      });
    e.preventDefault();
  })
});

</script>
@endsection
