@extends('master')
@section('title','Timesheet List')
@section('pagename','Timesheets')
@section('contents')
<section class="content-header">
  <h1>@yield('pagename')</h1>
</section>
<!-- Main content -->
<section class="content">
<div class="row">
  <div class="col-xs-12">
    @include('pages.partials.success')
    <div class="box box-primary">
      <div class="box-header">
        <a href='/timesheets/create' class='btn btn-primary'><i class="fa fa-plus"></i> Add More</a>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table id="timesheetslist" class="table table-bordered table-hover">
          <thead>
            <tr>
              <th>Timesheet Date</th>
              <th>Shop Name</th>
              <th class='text-center'>Action</th>
            </tr>
          </thead>
          <tbody>
            @if($timesheets->count()==0)
              <tr><td colspan='5' align='center'><h4>Records not Found</h4></td></tr>
            @else
              @foreach ($timesheets as $timesheet)
                <tr>
                  <td>{{date('Y-m-d (l)',strtotime($timesheet->date))}}</td>
                  <td>{{$timesheet->shops['0']->name}}</td>
                  <td align='center'><a title='Edit' class='purple' href="/timesheets/edit/{{$timesheet->id}}"><i class='fa fa-pencil'></i></a></td>
                </tr>
              @endforeach
            @endif
          </tbody>
        </table>
      </div>
      <!-- /.box-body -->
    </div>
  </div>
</div>
</section>
@endsection
@section('script')
<script type="text/javascript">
$(document).ready(function(){
  $('.status_confirm').click(function(e){
    if(!confirm('Are you sure you want to change the user\'s status ?')){
      e.preventDefault();
      return false;
    }
  });
});
</script>
@endsection
