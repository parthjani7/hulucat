<div class="modal_wrapper">
  <div class="modal timesheet_modal">
    <div class="modal-dialog modal-lg">
    <form action='/timesheets/create{{isset($timesheet->id)?'/'.$timesheet->id:''}}' method="post">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">@yield('pagename')</h4>
        </div>
        <div class="modal-body">
            <div class="row new_timesheet">
              <div class="col-md-3">
                <div class="form-group">
                  <label>Select Staff</label>
                  <select class="form-control" required id="select_staff" name="user_id">
                  </select>
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <label>Select Role</label>
                  <select class="form-control" required id="select_role" name="role_id">
                  </select>
                </div>
              </div>
              <div class="col-md-2">
                <div class="bootstrap-timepicker">
                  <div class="form-group">
                    <label>Start Time</label>
                    <div class="input-group">
                      <input type="text" class="form-control timepicker" readonly id="timepicker1">
                      <div class="input-group-addon">
                        <i class="fa fa-clock-o"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-2">
                <div class="bootstrap-timepicker">
                  <div class="form-group">
                    <label>End Time</label>
                    <div class="input-group">
                      <input type="text" class="form-control timepicker" readonly id="timepicker2">
                      <div class="input-group-addon">
                        <i class="fa fa-clock-o"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-1">
                <div class="form-group">
                  <label>Break</label>
                  <select class="form-control" required id="break_time" name="break">
                    <option>00</option>
                    <option>05</option>
                    <option>10</option>
                    <option>15</option>
                    <option>30</option>
                  </select>
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <label>Remark</label>
                  <input type="text" class="form-control" name="remark" placeholder="remark">
                </div>
              </div>
            </div>
            <input type="hidden" name="date" id="timesheet_date">
            <input type="hidden" name="shop_id" id="shop_id">
            {{csrf_field()}}
            @if(isset($timesheet->id))
              {{method_field('patch')}}
            @endif

        </div>
        <div class="modal-footer">
          <button type="submit" class='pull-left btn btn-success' name="button">Add</button>
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
        </div>
      </div>
      </form>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->
</div>
<!-- /.example-modal -->
