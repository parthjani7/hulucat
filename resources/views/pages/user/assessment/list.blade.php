@extends('master')
@section('title','Assessment List')
@section('pagename','Assessments')
@section('contents')
<section class="content-header">
  <h1>@yield('pagename')</h1>
</section>
<!-- Main content -->
<section class="content">
<div class="row">
  <div class="col-xs-12">
    @include('pages.partials.success')
    <div class="box box-primary">
      <!-- /.box-header -->
      <div class="box-body">
        <!-- @if(isset($status))
          <div class="alert alert-success">
            {{$status}}
          </div>
        @endif -->
        <table id="assessmentslist" class="table table-bordered table-hover">
          <thead>
            <tr>
              <th>#</th>
              <th>Assessment Name</th>
              <th>Total Questions</th>
              <th class='text-center'>Action</th>
            </tr>
          </thead>
          <tbody>
            @if($assessments->count()==0)
              <tr><td colspan='5' align='center'><h4>Records not Found</h4></td></tr>
            @else
              @foreach ($assessments as $assessment)
                <tr>
                  <td>{{$loop->index+1}}</td>
                  <td>{{$assessment->name}}</td>
                  <td>{{$assessment->total_questions}}</td>
                  <td align='center'>
                    <a title='Edit' class='purple' href="/assessments/take/{{$assessment->id}}">Take</a>
                  </td>
                </tr>
              @endforeach
            @endif
          </tbody>
        </table>
        {{csrf_field()}}
      </div>
      <!-- /.box-body -->
    </div>
  </div>
</div>
</section>
@endsection
@section('script')
<script type="text/javascript">
$(document).ready(function(){
  $('.status_confirm').click(function(e){
    if(!confirm('Are you sure you want to change the user\'s status ?')){
      e.preventDefault();
      return false;
    }
  });
});
</script>
@endsection
