@extends('master')
@section('title','Assessment is being taken')
@section('pagename','Assessment is being taken')
@section('headerscript')
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="/plugins/iCheck/all.css">
@endsection
@section('contents')
<section class="content-header">
  <h1>@yield('pagename')</h1>
</section>
<!-- Main content -->
<section class="content">

  @include('pages.partials.errors')
  @include('pages.partials.success')

  <form role="form" method='post'>
    <div class="row">
      <div class="col-md-12">
        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title">Please answer following questions</h3>
          </div>
          <div class="box-body">
            <div class="form-group">
              <div id="question_set">
                @foreach ($adata as $questions)
                  <?php $decode=json_decode($questions->question); ?>
                    <div class="question">
                      <div class="input-group">
                        <div class="input-group-addon que_num">{{$loop->index+1}}</div>
                        <input required disabled class="form-control" value="{{$decode->que[0]}}" type="text">
                      </div>
                      <br>
                      @if($questions->question_type==0)
                        <div class="form-group mcq">
                          <div class="row">
                          @foreach ($decode->options as $option)
                            <div class="col-md-3">
                              <label class="radio-inline">
                                <input type="radio" required name="questions[{{$questions->id}}]" value="{{$option}}"> {{$option}}
                              </label>
                            </div>
                          @endforeach
                          </div>
                        </div>
                      @else
                        <div class="row">
                          <div class="col-sm-12">
                            <div class="form-group">
                              <input required name="questions[{{$questions->id}}]" placeholder='Answer' class="form-control" type="text">
                            </div>
                          </div>
                        </div>
                      @endif
                    </div>
                    <hr>
                @endforeach
                <button type='submit' name="btnAdd" class="btn btn-success btn-md submit" id="btnAdd">Submit Assessment</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    {{csrf_field()}}
  </form>
</section>
@endsection
@section('footerscript')
<script src="/plugins/iCheck/icheck.min.js"></script>
@endsection
