@extends('master')
@section('title','Create EOD')
@section('pagename','Add EOD')
@section('headerscript')
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="/plugins/iCheck/all.css">
<style media="screen">
  .table-condensed > tbody > tr > td{padding:2px;}
  .box-header{padding:7px;}
  .box{margin-bottom:10px;}
  .form-group{margin-bottom:4px;}
</style>
@endsection
@section('contents')
<section class="content-header">
  <h1>@yield('pagename')</h1>

</section>
<!-- Main content -->
<section class="content">

  @include('pages.partials.errors')
  @include('pages.partials.success')

  <form role="form" action='{{route('sale.store')}}' method='post'>
    <div class="row">
      <div class="col-md-6">
        <div class="row">
          <div class="col-md-12">
            <div class="box box-info">
              <div class="box-header with-border">
                <h3 class="box-title">Sale Summary</h3>
                <span class='pull-right'><u><strong>@{{date}} - @{{day}}</strong></u></span>
              </div>
              <div class="box-body">
                <div class="form-group">
                  <input type="text" name="date" class="form-control" value="{{Date('Y-m-d')}}" disabled/>
                </div>
                <div class="form-group">
                  <label>Shop</label>
                  <select required v-on:change="getPaymentMethods()" v-model="shop" name="shop" class="form-control">
                    <option selected value="">Select Shop</option>
                    @foreach ($shops as $shop)
                      <option value="{{$shop->id}}">{{$shop->name}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group">
                  <input required name="opening_float" placeholder="Opening Float" class="form-control" />
                </div>

                <div class="form-group" v-for="payment_method in payment_methods">
                  <input required v-model.number="payment_method.value" :name="'payment_methods['+`${payment_method.id}`+'][]'" :placeholder="payment_method.name" class="form-control" />
                </div>

                <div class="form-group">
                  <label>Total Sales</label>
                  <input required v-model.number="total_sales" name="total_sales" placeholder="Total Sales" class="form-control" />
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="box box-info">
              <div class="box-header with-border">
                <h3 class="box-title">Cash Payments /Disbursements from Till</h3>
              </div>
              <div class="box-body">
                <table class="table table-condensed table-striped">
                </table>
                <button type='button' v-on:click='calculateTotal()' class="btn btn-success btn-md submit">Add Sale</button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="row">
          <div class="col-md-12">
            <div class="box box-info">
              <div class="box-header with-border">
                <h3 class="box-title">Actual Cash Count</h3>
              </div>
              <div class="box-body">
                <table class="table table-condensed table-striped">
                  <tr>
                    <th>Unit</th>
                    <th>Qty</th>
                    <th>Amount</th>
                  </tr>
                  <tr v-for='cash in cashCount'>
                    <td>@{{cash.unit}}</td>
                    <td><input :name="'cash_count['+`${cash.unit}`+'][]'" placeholder="0.00" v-model="cash.qty"></td>
                    <td>@{{isNaN(cash.qty)?'0.00':(cash.unit * cash.qty).toFixed(2)}}
                      <input type="hidden" v-model="cash.total" value="isNaN(cash.qty)?0:(cash.unit * cash.qty).toFixed(2)">
                    </td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="box box-info">
              <div class="box-body">
                <div class="form-group">
                  <label>Discount</label>
                  <input type="text" name="discount" class="form-control" value="0.00" />
                </div>
                <div class="form-group">
                  <label>Balance</label>
                  <input type="text" name="balance" class="form-control" value="0.00" />
                </div>
                <div class="form-group">
                  <textarea name="remarks" class="form-control" placeholder="Enter Remarks (Optional)"></textarea>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    {{csrf_field()}}
  </form>
</section>
@endsection
@section('footerscript')
<script src="/plugins/iCheck/icheck.min.js"></script>
<script type="text/javascript">
$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
  checkboxClass: 'icheckbox_minimal-blue',
  radioClass: 'iradio_minimal-blue'
});

var app=new Vue({
  el:'#app',
  data:{
    date:"{{Date('M d, Y')}}",
    day:"{{ucfirst(Date('l'))}}",
    payment_methods:[],
    shop:"",
    total_sales:0,
    cashCount:[{unit:0.1},{unit:0.2},{unit:0.5},{unit:1},{unit:2},
      {unit:5},{unit:10},{unit:20},{unit:50},{unit:100}],
    cash:[]
  },
  methods:{

    getPaymentMethods:function(){
      var Vue=this;
      swal({title: 'Please wait...',showConfirmButton: false});
      $.ajax({
				headers: {'X-CSRF-TOKEN': $('input[name="_token"]').val()},
				url:'/payment_methods/find/shop/'+Vue.shop,
				method: 'post',
				success:function(data){
          swal.close();
					if(data.resultCode=='error'){
            Vue.payment_methods=[];
						sweetAlert("Oops...", data.resultValue, "error");
            Vue.shop="";
					}else if(data.resultCode=='success'){
						Vue.payment_methods=data.resultValue;
					}
				},error:function(data){
					sweetAlert("Oops...", "Error occured, Please try later..!", "error");
				}
			});
    },
    calculateTotal:function(){
      console.info(this.cashCount);
    }
  },
  computed:{
    totalPayment:function(){
      var tmp=0;
      for(var i=0;i<this.payment_methods.length;i++){
        if(isNaN(this.payment_methods[i].value))
          continue;
        tmp+=this.payment_methods[i].value;
      }
      return tmp;
    },
    // total:function(){
    //   for(var i=0;i<this.cashCount.length;i++){
    //     if(isNaN(this.cashCount[i].qty))
    //       continue;
    //     this.cashCount[i].total=this.cashCount[i].qty*this.cashCount[i].unit;
    //   }
    //
    // }
  }
});
</script>
@endsection
