@extends('master')
@section('title','Edit Shop')
@section('pagename','Shop Information')
@section('contents')
<section class="content-header">
  <h1>@yield('pagename')</h1>
</section>
<!-- Main content -->
<section class="content">
    @include('pages.partials.errors')
  <form role="form" action='/shops/{{$shop->id}}/update' method='post'>
    <div class="row">
      <div class="col-md-12">
        <div class="box box-success">
          <div class="box-body">
            <div class="form-group">
              <label>Shop Name</label>
              <div class="input-group">
                <div class="input-group-addon"><i class="ion ion-beer"></i></div>
                <input id="name" required name="name" placeholder='Enter Shop Name' class="form-control" value="{{$shop->name}}" type="text">
              </div>
            </div>
            <div class="form-group">
              <label>Shop Status : </label>&nbsp;
              <label>
                <input type="radio" {{($shop->status=='1')?'checked':''}} required name="status" value='1' class="minimal"> Active
              </label>
              <label>
                <input type="radio" {{($shop->status=='0')?'checked':''}} required name="status" class="minimal" value='0'> Inactive
              </label>
            </div>
            <button type='submit' name="btnUpdate" class="btn btn-success btn-md submit" id="btnUpdate">Update Shop</button>
          </div>
        </div>
      </div>
    </div>
    {{ csrf_field() }}
    {{ method_field('PATCH') }}
  </form>
</section>
@endsection
