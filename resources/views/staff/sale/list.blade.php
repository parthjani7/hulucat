@extends('master')
@section('title','Shop List')
@section('pagename','Shops')
@section('contents')
<section class="content-header">
  <h1>@yield('pagename')</h1>
</section>
<!-- Main content -->
<section class="content">
<div class="row">
  <div class="col-xs-12">
    @include('pages.partials.success')
    <div class="box box-primary">
      <div class="box-header">
        <!-- <h3 class="box-title">Hover Data Table</h3> -->
        <a href='/shops/create' class='btn btn-primary'><i class="fa fa-plus"></i> Add More</a>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <!-- @if(isset($status))
          <div class="alert alert-success">
            {{$status}}
          </div>
        @endif -->
        <table id="shopslist" class="table table-bordered table-hover">
          <thead>
            <tr>
              <th>Shop Name</th>
              <th>Status</th>
              <th>Creation Date</th>
              <th class='text-center'>Action</th>
            </tr>
          </thead>
          <tbody>
            @if($shops->count()==0)
              <tr><td colspan='5' align='center'><h4>Records not Found</h4></td></tr>
            @else
              @foreach ($shops as $shop)
                <tr>
                  <td>{{$shop->name}}</td>
                  <td>
                    <a href='{{route('shop_toggle',$shop->id)}}' class='toggleStatus' title='click to toggle'>
                      <span class="label bg-{{($shop->status=='1')?'green':'red'}}">
                        {{($shop->status=='1')?'Active':'Inactive'}}
                      </span>
                    </a>
                  </td>
                  <td>{{$shop->created_at->toDateString()}}</td>
                  <td align='center'><a title='Edit' class='purple' href="shops/edit/{{$shop->id}}"><i class='fa fa-pencil'></i></a></td>
                </tr>
              @endforeach
            @endif
          </tbody>
        </table>
        {{csrf_field()}}
      </div>
      <!-- /.box-body -->
    </div>
  </div>
</div>
</section>
@endsection
@section('script')
<script type="text/javascript">
$(document).ready(function(){
  $('.status_confirm').click(function(e){
    if(!confirm('Are you sure you want to change the user\'s status ?')){
      e.preventDefault();
      return false;
    }
  });
});
</script>
@endsection
