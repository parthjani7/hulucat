<?php

// Auth Controller
Route::get('/',function(){ return redirect()->route('login'); });

Route::middleware('guest')->group(function(){
  Route::get('/login', 'Auth\AuthController@index')->name('login');
  Route::post('/login', 'Auth\AuthController@login');
});

Route::get('/logout', 'Auth\AuthController@logout')->name('logout');


//Common Tasks
Route::middleware('auth')->group(function(){

  Route::get('/dashboard','CommonController@dashboard')->name('dashboard');
  Route::get('/home',function(){ return redirect()->route('dashboard'); });

  //Profile
  Route::group(['prefix'=>'profile'],function(){
    Route::get('/', 'CommonController@profile')->name('profile');
    Route::patch('/update', 'CommonController@updateProfile');
  });

});

Route::middleware('admin_and_manager')->group(function(){
  Route::get('/staff','StaffController@index')->name('staff.index');
});

Route::middleware('admin_only')->group(function(){
  // Staff
  Route::group(['prefix'=>'staff'],function(){
    Route::post('/','StaffController@store')->name('staff.store');
    Route::get('/create','StaffController@create')->name('staff.create');
    Route::put('/{staff}','StaffController@update')->name('staff.update');
    Route::delete('/{staff}','StaffController@destroy')->name('staff.destroy');
    Route::get('/{staff}/edit','StaffController@edit')->name('staff.edit');
    Route::post('/toggle/{user}', 'StaffController@toggle')->name('staff_toggle');
  });

  // Shop
  Route::group(['prefix'=>'shops'],function(){
    Route::get('/','ShopsController@index')->name('shops');
    Route::post('/','ShopsController@store');
    Route::get('/create','ShopsController@create');
    Route::get('/edit/{shop}','ShopsController@edit');
    Route::patch('/{shop}/update','ShopsController@update');
    Route::get('/assign','ShopsController@assignShop');
    Route::post('/assign','ShopsController@updateAssignShop');
    Route::post('/toggle/{shop}', 'ShopsController@toggle')->name('shop_toggle');
  });

  //Roles & Permission
  Route::group(['prefix'=>'roles'],function(){
    Route::get('/', 'RoleController@index')->name('roles');
    Route::get('/create', 'RoleController@create');
    Route::post('/', 'RoleController@store');
    Route::get('/edit/{role}', 'RoleController@edit');
    Route::patch('/{role}/update', 'RoleController@update');
    Route::get('/grant', 'RoleController@showRoles');
    Route::post('/grant/{user}', 'RoleController@grantRoles');
    Route::post('/toggle/{role}', 'RoleController@toggle')->name('role_toggle');
  });

  //Positions
  Route::group(['prefix'=>'positions'],function(){
    Route::get('/', 'PositionController@index')->name('positions');
    Route::get('/create', 'PositionController@create');
    Route::post('/', 'PositionController@store');
    Route::get('/edit/{position}', 'PositionController@edit');
    Route::patch('/{position}/update', 'PositionController@update');
    Route::get('/assign','PositionController@assignPosition');
    Route::post('/assign','PositionController@updatePosition');
    Route::post('/toggle/{position}', 'PositionController@toggle')->name('position_toggle');
  });

  //Payment Methods
  //Route::resource('/payment_methods','PaymentMethodController');

  Route::group(['prefix'=>'payment_methods'],function(){
    Route::get('/', 'PaymentMethodController@index')->name('payment_methods.index');
    Route::post('/', 'PaymentMethodController@store')->name('payment_methods.store');
    Route::get('/create', 'PaymentMethodController@create')->name('payment_methods.create');

    Route::get('/assign', 'PaymentMethodController@showAssign')->name('payment_methods.showAssign');
    Route::post('/assign', 'PaymentMethodController@assign')->name('payment_methods.assign');
    Route::patch('/{payment_method}', 'PaymentMethodController@update')->name('payment_methods.update');
    Route::patch('/{payment_method}/edit', 'PaymentMethodController@edit')->name('payment_methods.edit');
    Route::delete('/{payment_method}', 'PaymentMethodController@destroy')->name('payment_methods.destroy');
    Route::post('/toggle/{payment_method}', 'PaymentMethodController@toggle')->name('payment_method_toggle');
  });

});

//Timesheet
Route::group(['prefix'=>'timesheets'],function(){
  Route::get('/', 'TimesheetController@index')->name('timesheets');
  Route::get('/create', 'TimesheetController@create');
  Route::post('/create', 'TimesheetController@store');
  Route::get('/edit/{timesheet}', 'TimesheetController@edit');
  Route::patch('/create/{timesheet}', 'TimesheetController@update');
  Route::post('/find/staff/{shop}', 'TimesheetController@findStaff');
  Route::post('/record/{record}', 'TimesheetController@findRecord');
  Route::get('/record/{record}', 'TimesheetController@findRecord');
  Route::post('/delete/record/{record}', 'TimesheetController@destroyRecord');
  Route::post('/find/positions/{user}', 'TimesheetController@findPosition');
});

//Assessments
Route::group(['prefix'=>'assessments'],function(){
  Route::get('/', 'AssessmentController@index')->name('assessments');
  Route::get('/create', 'AssessmentController@create');
  Route::post('/create', 'AssessmentController@store');
  Route::get('/assign', 'AssessmentController@assignAssessment');
  Route::post('/assign', 'AssessmentController@updateAssignment');

  Route::get('/edit/{assessment}', 'AssessmentController@edit');
  Route::patch('/edit/{assessment}', 'AssessmentController@update');
  Route::get('/resultlist', 'AssessmentController@resultList');
  Route::post('/toggle/{assessment}', 'AssessmentController@toggle')->name('assessment_toggle');
  Route::get('/result/{user}/{assessment}', 'AssessmentController@getResult');
  Route::post('/result/change', 'AssessmentController@toggleAnswer')->name('change_answer');

  //for staff
  Route::get('/take', 'UserController@getAssessments');
  Route::get('/take/{assessment}', 'UserController@takeAssessment');
  Route::post('/take/{assessment}', 'UserController@submitAssessment');

});
Route::group(['prefix'=>'payment_methods'],function(){
  Route::post('/find/shop/{shop}', 'PaymentMethodController@getPaymentMethods')->name('payment_methods.find');
});

Route::group(['prefix'=>'eod'],function(){
  Route::resource('/sale','SaleController');
});