const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.copy(
   'node_modules/sweetalert2/dist/sweetalert2.min.css',
   'resources/assets/sass'
).copy(
   'node_modules/sweetalert2/dist/sweetalert2.min.js',
   'resources/assets/js'
);

mix.js('resources/assets/js/app.js', 'public/custom/js')
  .copy('resources/assets/js/sweetalert2.min.js', 'public/plugins/sweetalert2/js')
  .sass('resources/assets/sass/app.scss', 'public/custom/css')
  .sass('resources/assets/sass/sweetalert2.min.css', 'public/plugins/sweetalert2/css');